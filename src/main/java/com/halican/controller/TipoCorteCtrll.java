package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.TipoCorte;
import com.halican.dto.TipoCorteDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.ITipoCorteService;


@RestController
@RequestMapping("/tipocorte")
public class TipoCorteCtrll {
	@Autowired
	private ITipoCorteService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<TipoCorteDTO>> listar() throws Exception{				
		List<TipoCorteDTO> lista = service.listar().stream().map(p -> mapper.map(p, TipoCorteDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<TipoCorteDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		TipoCorteDTO dtoResponse;
		TipoCorte obj = service.listarPorId(id); //TipoCorte		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, TipoCorteDTO.class); //TipoCorteDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<TipoCorteDTO> registrar(@Valid @RequestBody TipoCorteDTO dtoRequest) throws Exception{
		TipoCorte p = mapper.map(dtoRequest, TipoCorte.class);
		TipoCorte obj = service.registrar(p);
		TipoCorteDTO dtoResponse = mapper.map(obj, TipoCorteDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody TipoCorteDTO dtoRequest) throws Exception{
		TipoCorte p = mapper.map(dtoRequest, TipoCorte.class);
		TipoCorte obj = service.registrar(p);
		TipoCorteDTO dtoResponse = mapper.map(obj, TipoCorteDTO.class);
		//localhost:8080/TipoCortes/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdtipocorte()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<TipoCorteDTO> modificar(@RequestBody TipoCorteDTO dtoRequest) throws Exception {
		TipoCorte pac = service.listarPorId(dtoRequest.getIdtipocorte());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdtipocorte());	
		}
		
		TipoCorte p = mapper.map(dtoRequest, TipoCorte.class);
		 
		TipoCorte obj = service.modificar(p);
		
		TipoCorteDTO dtoResponse = mapper.map(obj, TipoCorteDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		TipoCorte pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<TipoCorteDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		TipoCorte obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		TipoCorteDTO dto = mapper.map(obj, TipoCorteDTO.class);
		
		EntityModel<TipoCorteDTO> recurso = EntityModel.of(dto);
		//localhost:8080/TipoCortes/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("TipoCorte-recurso1"));
		recurso.add(link2.withRel("TipoCorte-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<TipoCorteDTO>> listarPageable(Pageable page) throws Exception{
//		Page<TipoCorteDTO> TipoCortes = service.listarPageable(page).map(p -> mapper.map(p, TipoCorteDTO.class));
//		
//		return new ResponseEntity<>(TipoCortes, HttpStatus.OK);
//	}
}
