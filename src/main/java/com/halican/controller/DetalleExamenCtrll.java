package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.DetalleExamen;
import com.halican.dto.DetalleExamenDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IDetalleExamenService;
import com.halican.service.IDetalleExamenService;

@RestController
@RequestMapping("/detalleexamen")
public class DetalleExamenCtrll {
	
	@Autowired
	private IDetalleExamenService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<DetalleExamenDTO>> listar() throws Exception{				
		List<DetalleExamenDTO> lista = service.listar().stream().map(p -> mapper.map(p, DetalleExamenDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<DetalleExamenDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		DetalleExamenDTO dtoResponse;
		DetalleExamen obj = service.listarPorId(id); //DetalleExamen		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, DetalleExamenDTO.class); //DetalleExamenDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<DetalleExamenDTO> registrar(@Valid @RequestBody DetalleExamenDTO dtoRequest) throws Exception{
		DetalleExamen p = mapper.map(dtoRequest, DetalleExamen.class);
		DetalleExamen obj = service.registrar(p);
		DetalleExamenDTO dtoResponse = mapper.map(obj, DetalleExamenDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody DetalleExamenDTO dtoRequest) throws Exception{
		DetalleExamen p = mapper.map(dtoRequest, DetalleExamen.class);
		DetalleExamen obj = service.registrar(p);
		DetalleExamenDTO dtoResponse = mapper.map(obj, DetalleExamenDTO.class);
		//localhost:8080/DetalleExamens/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIddetalleexamen()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<DetalleExamenDTO> modificar(@RequestBody DetalleExamenDTO dtoRequest) throws Exception {
		DetalleExamen pac = service.listarPorId(dtoRequest.getIddetalleexamen());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIddetalleexamen());	
		}
		
		DetalleExamen p = mapper.map(dtoRequest, DetalleExamen.class);
		 
		DetalleExamen obj = service.modificar(p);
		
		DetalleExamenDTO dtoResponse = mapper.map(obj, DetalleExamenDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		DetalleExamen pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<DetalleExamenDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		DetalleExamen obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		DetalleExamenDTO dto = mapper.map(obj, DetalleExamenDTO.class);
		
		EntityModel<DetalleExamenDTO> recurso = EntityModel.of(dto);
		//localhost:8080/DetalleExamens/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("DetalleExamen-recurso1"));
		recurso.add(link2.withRel("DetalleExamen-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<DetalleExamenDTO>> listarPageable(Pageable page) throws Exception{
//		Page<DetalleExamenDTO> DetalleExamens = service.listarPageable(page).map(p -> mapper.map(p, DetalleExamenDTO.class));
//		
//		return new ResponseEntity<>(DetalleExamens, HttpStatus.OK);
//	}

}
