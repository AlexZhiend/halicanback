package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Cirujia;
import com.halican.dto.CirujiaDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.ICirujiaService;
import com.halican.service.ICirujiaService;

@RestController
@RequestMapping("/cirujia")
public class CirujiaCtrll {
	
	@Autowired
	private ICirujiaService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<CirujiaDTO>> listar() throws Exception{				
		List<CirujiaDTO> lista = service.listar().stream().map(p -> mapper.map(p, CirujiaDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<CirujiaDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		CirujiaDTO dtoResponse;
		Cirujia obj = service.listarPorId(id); //Cirujia		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, CirujiaDTO.class); //CirujiaDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<CirujiaDTO> registrar(@Valid @RequestBody CirujiaDTO dtoRequest) throws Exception{
		Cirujia p = mapper.map(dtoRequest, Cirujia.class);
		Cirujia obj = service.registrar(p);
		CirujiaDTO dtoResponse = mapper.map(obj, CirujiaDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody CirujiaDTO dtoRequest) throws Exception{
		Cirujia p = mapper.map(dtoRequest, Cirujia.class);
		Cirujia obj = service.registrar(p);
		CirujiaDTO dtoResponse = mapper.map(obj, CirujiaDTO.class);
		//localhost:8080/Cirujias/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdcirujia()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<CirujiaDTO> modificar(@RequestBody CirujiaDTO dtoRequest) throws Exception {
		Cirujia pac = service.listarPorId(dtoRequest.getIdcirujia());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdcirujia());	
		}
		
		Cirujia p = mapper.map(dtoRequest, Cirujia.class);
		 
		Cirujia obj = service.modificar(p);
		
		CirujiaDTO dtoResponse = mapper.map(obj, CirujiaDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Cirujia pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<CirujiaDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Cirujia obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		CirujiaDTO dto = mapper.map(obj, CirujiaDTO.class);
		
		EntityModel<CirujiaDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Cirujias/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Cirujia-recurso1"));
		recurso.add(link2.withRel("Cirujia-recurso2"));
		
		return recurso;
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<CirujiaDTO>> listarPageable(Pageable page) throws Exception{
		Page<CirujiaDTO> cirujias = service.listarPageable(page).map(p -> mapper.map(p, CirujiaDTO.class));
		
		return new ResponseEntity<>(cirujias, HttpStatus.OK);
	}
	
}
