package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.RegistroDeHospedaje;
import com.halican.dto.RegistroDeHospedajeDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IRegistroHospedajeService;


@RestController
@RequestMapping("/registrodehospedaje")
public class RegistroHospedajeCtrll {
	
	@Autowired
	private IRegistroHospedajeService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<RegistroDeHospedajeDTO>> listar() throws Exception{				
		List<RegistroDeHospedajeDTO> lista = service.listar().stream().map(p -> mapper.map(p, RegistroDeHospedajeDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<RegistroDeHospedajeDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		RegistroDeHospedajeDTO dtoResponse;
		RegistroDeHospedaje obj = service.listarPorId(id); //RegistroDeHospedaje		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, RegistroDeHospedajeDTO.class); //RegistroDeHospedajeDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<RegistroDeHospedajeDTO> registrar(@Valid @RequestBody RegistroDeHospedajeDTO dtoRequest) throws Exception{
		RegistroDeHospedaje p = mapper.map(dtoRequest, RegistroDeHospedaje.class);
		RegistroDeHospedaje obj = service.registrar(p);
		RegistroDeHospedajeDTO dtoResponse = mapper.map(obj, RegistroDeHospedajeDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody RegistroDeHospedajeDTO dtoRequest) throws Exception{
		RegistroDeHospedaje p = mapper.map(dtoRequest, RegistroDeHospedaje.class);
		RegistroDeHospedaje obj = service.registrar(p);
		RegistroDeHospedajeDTO dtoResponse = mapper.map(obj, RegistroDeHospedajeDTO.class);
		//localhost:8080/RegistroDeHospedajes/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdregistrohospedaje()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<RegistroDeHospedajeDTO> modificar(@RequestBody RegistroDeHospedajeDTO dtoRequest) throws Exception {
		RegistroDeHospedaje pac = service.listarPorId(dtoRequest.getIdregistrohospedaje());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdregistrohospedaje());	
		}
		
		RegistroDeHospedaje p = mapper.map(dtoRequest, RegistroDeHospedaje.class);
		 
		RegistroDeHospedaje obj = service.modificar(p);
		
		RegistroDeHospedajeDTO dtoResponse = mapper.map(obj, RegistroDeHospedajeDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		RegistroDeHospedaje pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<RegistroDeHospedajeDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		RegistroDeHospedaje obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		RegistroDeHospedajeDTO dto = mapper.map(obj, RegistroDeHospedajeDTO.class);
		
		EntityModel<RegistroDeHospedajeDTO> recurso = EntityModel.of(dto);
		//localhost:8080/RegistroDeHospedajes/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("RegistroDeHospedaje-recurso1"));
		recurso.add(link2.withRel("RegistroDeHospedaje-recurso2"));
		
		return recurso;
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<RegistroDeHospedajeDTO>> listarPageable(Pageable page) throws Exception{
		Page<RegistroDeHospedajeDTO> RegistroDeHospedajes = service.listarPageable(page).map(p -> mapper.map(p, RegistroDeHospedajeDTO.class));
		
		return new ResponseEntity<>(RegistroDeHospedajes, HttpStatus.OK);
	}
}
