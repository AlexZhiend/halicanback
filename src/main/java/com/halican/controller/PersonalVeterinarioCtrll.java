package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.PersonalVeterinario;
import com.halican.dto.PersonalVeterinarioDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IPersonalVeterinarioService;


@RestController
@RequestMapping("/personalveterinario")
public class PersonalVeterinarioCtrll {

	@Autowired
	private IPersonalVeterinarioService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<PersonalVeterinarioDTO>> listar() throws Exception{				
		List<PersonalVeterinarioDTO> lista = service.listar().stream().map(p -> mapper.map(p, PersonalVeterinarioDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<PersonalVeterinarioDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		PersonalVeterinarioDTO dtoResponse;
		PersonalVeterinario obj = service.listarPorId(id); //PersonalVeterinario		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, PersonalVeterinarioDTO.class); //PersonalVeterinarioDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<PersonalVeterinarioDTO> registrar(@Valid @RequestBody PersonalVeterinarioDTO dtoRequest) throws Exception{
		PersonalVeterinario p = mapper.map(dtoRequest, PersonalVeterinario.class);
		PersonalVeterinario obj = service.registrar(p);
		PersonalVeterinarioDTO dtoResponse = mapper.map(obj, PersonalVeterinarioDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody PersonalVeterinarioDTO dtoRequest) throws Exception{
		PersonalVeterinario p = mapper.map(dtoRequest, PersonalVeterinario.class);
		PersonalVeterinario obj = service.registrar(p);
		PersonalVeterinarioDTO dtoResponse = mapper.map(obj, PersonalVeterinarioDTO.class);
		//localhost:8080/PersonalVeterinarios/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdpersonal()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<PersonalVeterinarioDTO> modificar(@RequestBody PersonalVeterinarioDTO dtoRequest) throws Exception {
		PersonalVeterinario pac = service.listarPorId(dtoRequest.getIdpersonal());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdpersonal());	
		}
		
		PersonalVeterinario p = mapper.map(dtoRequest, PersonalVeterinario.class);
		 
		PersonalVeterinario obj = service.modificar(p);
		
		PersonalVeterinarioDTO dtoResponse = mapper.map(obj, PersonalVeterinarioDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		PersonalVeterinario pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<PersonalVeterinarioDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		PersonalVeterinario obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		PersonalVeterinarioDTO dto = mapper.map(obj, PersonalVeterinarioDTO.class);
		
		EntityModel<PersonalVeterinarioDTO> recurso = EntityModel.of(dto);
		//localhost:8080/PersonalVeterinarios/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("PersonalVeterinario-recurso1"));
		recurso.add(link2.withRel("PersonalVeterinario-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<PersonalVeterinarioDTO>> listarPageable(Pageable page) throws Exception{
//		Page<PersonalVeterinarioDTO> PersonalVeterinarios = service.listarPageable(page).map(p -> mapper.map(p, PersonalVeterinarioDTO.class));
//		
//		return new ResponseEntity<>(PersonalVeterinarios, HttpStatus.OK);
//	}
}
