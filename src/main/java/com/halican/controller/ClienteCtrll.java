package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Cliente;
import com.halican.dto.ClienteDTO;
import com.halican.dto.SearchClienteDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IClienteService;



@RestController
@RequestMapping("/cliente")
public class ClienteCtrll {
	@Autowired
	private IClienteService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<ClienteDTO>> listar() throws Exception{				
		List<ClienteDTO> lista = service.listar().stream().map(p -> mapper.map(p, ClienteDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ClienteDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		ClienteDTO dtoResponse;
		Cliente obj = service.listarPorId(id); //Cliente		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, ClienteDTO.class); //ClienteDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<ClienteDTO> registrar(@Valid @RequestBody ClienteDTO dtoRequest) throws Exception{
		Cliente p = mapper.map(dtoRequest, Cliente.class);
		Cliente obj = service.registrar(p);
		ClienteDTO dtoResponse = mapper.map(obj, ClienteDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody ClienteDTO dtoRequest) throws Exception{
		Cliente p = mapper.map(dtoRequest, Cliente.class);
		Cliente obj = service.registrar(p);
		ClienteDTO dtoResponse = mapper.map(obj, ClienteDTO.class);
		//localhost:8080/Clientes/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdcliente()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<ClienteDTO> modificar(@RequestBody ClienteDTO dtoRequest) throws Exception {
		Cliente pac = service.listarPorId(dtoRequest.getIdcliente());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdcliente());	
		}
		
		Cliente p = mapper.map(dtoRequest, Cliente.class);
		 
		Cliente obj = service.modificar(p);
		
		ClienteDTO dtoResponse = mapper.map(obj, ClienteDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Cliente pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<ClienteDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Cliente obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		ClienteDTO dto = mapper.map(obj, ClienteDTO.class);
		
		EntityModel<ClienteDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Clientes/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Cliente-recurso1"));
		recurso.add(link2.withRel("Cliente-recurso2"));
		
		return recurso;
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<ClienteDTO>> listarPageable(Pageable page) throws Exception{
		Page<ClienteDTO> Clientes = service.listarPageable(page).map(p -> mapper.map(p, ClienteDTO.class));
		
		return new ResponseEntity<>(Clientes, HttpStatus.OK);
	}
	
	@PostMapping("/buscar/nombreodni")
	public ResponseEntity<List<ClienteDTO>> busquedaClientePorDnioNombre(@RequestBody SearchClienteDTO parametro) throws Exception{				
		List<Cliente> clientes = new ArrayList<>();

		clientes = service.BuscarCliente(parametro.getNombreodni());
		
		List<ClienteDTO> clientesDTO = mapper.map(clientes, new TypeToken<List<ClienteDTO>>() {}.getType());

		return new ResponseEntity<List<ClienteDTO>>(clientesDTO, HttpStatus.OK);
	}
}
