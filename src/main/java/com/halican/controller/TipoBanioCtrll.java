package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.TipoBanio;
import com.halican.dto.TipoBanioDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.ITipoBanioService;

@RestController
@RequestMapping("/tipobanio")
public class TipoBanioCtrll {
	
	@Autowired
	private ITipoBanioService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<TipoBanioDTO>> listar() throws Exception{				
		List<TipoBanioDTO> lista = service.listar().stream().map(p -> mapper.map(p, TipoBanioDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<TipoBanioDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		TipoBanioDTO dtoResponse;
		TipoBanio obj = service.listarPorId(id); //TipoBanio		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, TipoBanioDTO.class); //TipoBanioDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<TipoBanioDTO> registrar(@Valid @RequestBody TipoBanioDTO dtoRequest) throws Exception{
		TipoBanio p = mapper.map(dtoRequest, TipoBanio.class);
		TipoBanio obj = service.registrar(p);
		TipoBanioDTO dtoResponse = mapper.map(obj, TipoBanioDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody TipoBanioDTO dtoRequest) throws Exception{
		TipoBanio p = mapper.map(dtoRequest, TipoBanio.class);
		TipoBanio obj = service.registrar(p);
		TipoBanioDTO dtoResponse = mapper.map(obj, TipoBanioDTO.class);
		//localhost:8080/TipoBanios/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdtipobanio()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<TipoBanioDTO> modificar(@RequestBody TipoBanioDTO dtoRequest) throws Exception {
		TipoBanio pac = service.listarPorId(dtoRequest.getIdtipobanio());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdtipobanio());	
		}
		
		TipoBanio p = mapper.map(dtoRequest, TipoBanio.class);
		 
		TipoBanio obj = service.modificar(p);
		
		TipoBanioDTO dtoResponse = mapper.map(obj, TipoBanioDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		TipoBanio pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<TipoBanioDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		TipoBanio obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		TipoBanioDTO dto = mapper.map(obj, TipoBanioDTO.class);
		
		EntityModel<TipoBanioDTO> recurso = EntityModel.of(dto);
		//localhost:8080/TipoBanios/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("TipoBanio-recurso1"));
		recurso.add(link2.withRel("TipoBanio-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<TipoBanioDTO>> listarPageable(Pageable page) throws Exception{
//		Page<TipoBanioDTO> TipoBanios = service.listarPageable(page).map(p -> mapper.map(p, TipoBanioDTO.class));
//		
//		return new ResponseEntity<>(TipoBanios, HttpStatus.OK);
//	}
}
