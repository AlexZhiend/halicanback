package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.TipoComprobante;
import com.halican.dto.TipoComprobanteDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.ITipoComprobanteService;

@RestController
@RequestMapping("/tipocomprobante")
public class TipoComprobanteCtrll {

	@Autowired
	private ITipoComprobanteService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<TipoComprobanteDTO>> listar() throws Exception{				
		List<TipoComprobanteDTO> lista = service.listar().stream().map(p -> mapper.map(p, TipoComprobanteDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<TipoComprobanteDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		TipoComprobanteDTO dtoResponse;
		TipoComprobante obj = service.listarPorId(id); //TipoComprobante		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, TipoComprobanteDTO.class); //TipoComprobanteDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<TipoComprobanteDTO> registrar(@Valid @RequestBody TipoComprobanteDTO dtoRequest) throws Exception{
		TipoComprobante p = mapper.map(dtoRequest, TipoComprobante.class);
		TipoComprobante obj = service.registrar(p);
		TipoComprobanteDTO dtoResponse = mapper.map(obj, TipoComprobanteDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody TipoComprobanteDTO dtoRequest) throws Exception{
		TipoComprobante p = mapper.map(dtoRequest, TipoComprobante.class);
		TipoComprobante obj = service.registrar(p);
		TipoComprobanteDTO dtoResponse = mapper.map(obj, TipoComprobanteDTO.class);
		//localhost:8080/TipoComprobantes/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdtipocomprobante()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<TipoComprobanteDTO> modificar(@RequestBody TipoComprobanteDTO dtoRequest) throws Exception {
		TipoComprobante pac = service.listarPorId(dtoRequest.getIdtipocomprobante());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdtipocomprobante());	
		}
		
		TipoComprobante p = mapper.map(dtoRequest, TipoComprobante.class);
		 
		TipoComprobante obj = service.modificar(p);
		
		TipoComprobanteDTO dtoResponse = mapper.map(obj, TipoComprobanteDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		TipoComprobante pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<TipoComprobanteDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		TipoComprobante obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		TipoComprobanteDTO dto = mapper.map(obj, TipoComprobanteDTO.class);
		
		EntityModel<TipoComprobanteDTO> recurso = EntityModel.of(dto);
		//localhost:8080/TipoComprobantes/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("TipoComprobante-recurso1"));
		recurso.add(link2.withRel("TipoComprobante-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<TipoComprobanteDTO>> listarPageable(Pageable page) throws Exception{
//		Page<TipoComprobanteDTO> TipoComprobantes = service.listarPageable(page).map(p -> mapper.map(p, TipoComprobanteDTO.class));
//		
//		return new ResponseEntity<>(TipoComprobantes, HttpStatus.OK);
//	}
	
	
	@GetMapping("/buscarporcodigo/{codigosunat}")
	public ResponseEntity<TipoComprobanteDTO> Buscarporcodigosunat(@PathVariable("codigosunat") String codigosunat) throws Exception{
		TipoComprobanteDTO dtoResponse;
		TipoComprobante obj = service.BuscarPorCodigo(codigosunat); //TipoComprobante		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + codigosunat);
		}else {
			dtoResponse = mapper.map(obj, TipoComprobanteDTO.class); //TipoComprobanteDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
}
