package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.TipoAfectacion;
import com.halican.dto.TipoAfectacionDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.ITipoAfectacionService;


@RestController
@RequestMapping("/tipoafectacion")
public class TipoAfectacionCtrll {

	@Autowired
	private ITipoAfectacionService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<TipoAfectacionDTO>> listar() throws Exception{				
		List<TipoAfectacionDTO> lista = service.listar().stream().map(p -> mapper.map(p, TipoAfectacionDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<TipoAfectacionDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		TipoAfectacionDTO dtoResponse;
		TipoAfectacion obj = service.listarPorId(id); //TipoAfectacion		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, TipoAfectacionDTO.class); //TipoAfectacionDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<TipoAfectacionDTO> registrar(@Valid @RequestBody TipoAfectacionDTO dtoRequest) throws Exception{
		TipoAfectacion p = mapper.map(dtoRequest, TipoAfectacion.class);
		TipoAfectacion obj = service.registrar(p);
		TipoAfectacionDTO dtoResponse = mapper.map(obj, TipoAfectacionDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody TipoAfectacionDTO dtoRequest) throws Exception{
		TipoAfectacion p = mapper.map(dtoRequest, TipoAfectacion.class);
		TipoAfectacion obj = service.registrar(p);
		TipoAfectacionDTO dtoResponse = mapper.map(obj, TipoAfectacionDTO.class);
		//localhost:8080/TipoAfectacions/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdtipoafectacion()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<TipoAfectacionDTO> modificar(@RequestBody TipoAfectacionDTO dtoRequest) throws Exception {
		TipoAfectacion pac = service.listarPorId(dtoRequest.getIdtipoafectacion());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdtipoafectacion());	
		}
		
		TipoAfectacion p = mapper.map(dtoRequest, TipoAfectacion.class);
		 
		TipoAfectacion obj = service.modificar(p);
		
		TipoAfectacionDTO dtoResponse = mapper.map(obj, TipoAfectacionDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		TipoAfectacion pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<TipoAfectacionDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		TipoAfectacion obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		TipoAfectacionDTO dto = mapper.map(obj, TipoAfectacionDTO.class);
		
		EntityModel<TipoAfectacionDTO> recurso = EntityModel.of(dto);
		//localhost:8080/TipoAfectacions/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("TipoAfectacion-recurso1"));
		recurso.add(link2.withRel("TipoAfectacion-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<TipoAfectacionDTO>> listarPageable(Pageable page) throws Exception{
//		Page<TipoAfectacionDTO> TipoAfectacions = service.listarPageable(page).map(p -> mapper.map(p, TipoAfectacionDTO.class));
//		
//		return new ResponseEntity<>(TipoAfectacions, HttpStatus.OK);
//	}
}
