package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.PacienteAnimal;
import com.halican.dto.PacienteAnimalDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IPacienteAnimalService;


@RestController
@RequestMapping("/pacienteanimal")
public class PacienteAnimalCtrll {
	@Autowired
	private IPacienteAnimalService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<PacienteAnimalDTO>> listar() throws Exception{				
		List<PacienteAnimalDTO> lista = service.listar().stream().map(p -> mapper.map(p, PacienteAnimalDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<PacienteAnimalDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		PacienteAnimalDTO dtoResponse;
		PacienteAnimal obj = service.listarPorId(id); //PacienteAnimal		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, PacienteAnimalDTO.class); //PacienteAnimalDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<PacienteAnimalDTO> registrar(@Valid @RequestBody PacienteAnimalDTO dtoRequest) throws Exception{
		PacienteAnimal p = mapper.map(dtoRequest, PacienteAnimal.class);
		PacienteAnimal obj = service.registrar(p);
		PacienteAnimalDTO dtoResponse = mapper.map(obj, PacienteAnimalDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody PacienteAnimalDTO dtoRequest) throws Exception{
		PacienteAnimal p = mapper.map(dtoRequest, PacienteAnimal.class);
		PacienteAnimal obj = service.registrar(p);
		PacienteAnimalDTO dtoResponse = mapper.map(obj, PacienteAnimalDTO.class);
		//localhost:8080/PacienteAnimals/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdpacienteanimal()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<PacienteAnimalDTO> modificar(@RequestBody PacienteAnimalDTO dtoRequest) throws Exception {
		PacienteAnimal pac = service.listarPorId(dtoRequest.getIdpacienteanimal());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdpacienteanimal());	
		}
		
		PacienteAnimal p = mapper.map(dtoRequest, PacienteAnimal.class);
		 
		PacienteAnimal obj = service.modificar(p);
		
		PacienteAnimalDTO dtoResponse = mapper.map(obj, PacienteAnimalDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		PacienteAnimal pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<PacienteAnimalDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		PacienteAnimal obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		PacienteAnimalDTO dto = mapper.map(obj, PacienteAnimalDTO.class);
		
		EntityModel<PacienteAnimalDTO> recurso = EntityModel.of(dto);
		//localhost:8080/PacienteAnimals/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("PacienteAnimal-recurso1"));
		recurso.add(link2.withRel("PacienteAnimal-recurso2"));
		
		return recurso;
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<PacienteAnimalDTO>> listarPageable(Pageable page) throws Exception{
		Page<PacienteAnimalDTO> PacienteAnimals = service.listarPageable(page).map(p -> mapper.map(p, PacienteAnimalDTO.class));
		
		return new ResponseEntity<>(PacienteAnimals, HttpStatus.OK);
	}
	
	
	@GetMapping("/buscarporcliente/{parametro}")
	public ResponseEntity<List<PacienteAnimalDTO>> listarPorCliente(@PathVariable("parametro") String parametro) throws Exception{				
		List<PacienteAnimalDTO> lista = service.BuscarPorCliente(parametro).stream().map(p -> mapper.map(p, PacienteAnimalDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/buscarpornombre/{parametro}")
	public ResponseEntity<List<PacienteAnimalDTO>> listarPorNombre(@PathVariable("parametro") String parametro) throws Exception{				
		List<PacienteAnimalDTO> lista = service.BuscarPornombre(parametro).stream().map(p -> mapper.map(p, PacienteAnimalDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
		
	
	@GetMapping("/buscarultimahistoria")
	public ResponseEntity<PacienteAnimalDTO> BuscarUltimaHistoria() throws Exception{
		PacienteAnimalDTO dtoResponse;
		PacienteAnimal obj = service.BuscarUltimaHistoria(); //PacienteAnimal		

		if(obj == null) {
			throw new ModeloNotFoundException("PACIENTE NO ENCONTRADO ");
		}else {
			dtoResponse = mapper.map(obj, PacienteAnimalDTO.class); //PacienteAnimalDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	
}
