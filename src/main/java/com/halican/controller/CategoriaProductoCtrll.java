package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.CategoriaProducto;
import com.halican.dto.CategoriaProductoDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.ICategoriaProductoService;

@RestController
@RequestMapping("/categoriaproducto")
public class CategoriaProductoCtrll {

	@Autowired
	private ICategoriaProductoService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<CategoriaProductoDTO>> listar() throws Exception{				
		List<CategoriaProductoDTO> lista = service.listar().stream().map(p -> mapper.map(p, CategoriaProductoDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<CategoriaProductoDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		CategoriaProductoDTO dtoResponse;
		CategoriaProducto obj = service.listarPorId(id); //CategoriaProducto		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, CategoriaProductoDTO.class); //CategoriaProductoDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<CategoriaProductoDTO> registrar(@Valid @RequestBody CategoriaProductoDTO dtoRequest) throws Exception{
		CategoriaProducto p = mapper.map(dtoRequest, CategoriaProducto.class);
		CategoriaProducto obj = service.registrar(p);
		CategoriaProductoDTO dtoResponse = mapper.map(obj, CategoriaProductoDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody CategoriaProductoDTO dtoRequest) throws Exception{
		CategoriaProducto p = mapper.map(dtoRequest, CategoriaProducto.class);
		CategoriaProducto obj = service.registrar(p);
		CategoriaProductoDTO dtoResponse = mapper.map(obj, CategoriaProductoDTO.class);
		//localhost:8080/CategoriaProductos/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdcategoriaproducto()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<CategoriaProductoDTO> modificar(@RequestBody CategoriaProductoDTO dtoRequest) throws Exception {
		CategoriaProducto pac = service.listarPorId(dtoRequest.getIdcategoriaproducto());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdcategoriaproducto());	
		}
		
		CategoriaProducto p = mapper.map(dtoRequest, CategoriaProducto.class);
		 
		CategoriaProducto obj = service.modificar(p);
		
		CategoriaProductoDTO dtoResponse = mapper.map(obj, CategoriaProductoDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		CategoriaProducto pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
		
}
