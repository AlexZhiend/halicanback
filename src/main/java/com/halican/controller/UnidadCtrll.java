package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Unidad;
import com.halican.dto.UnidadDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IUnidadService;

@RestController
@RequestMapping("/unidad")
public class UnidadCtrll {
	
	@Autowired
	private IUnidadService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<UnidadDTO>> listar() throws Exception{				
		List<UnidadDTO> lista = service.listar().stream().map(p -> mapper.map(p, UnidadDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<UnidadDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		UnidadDTO dtoResponse;
		Unidad obj = service.listarPorId(id); //Unidad		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, UnidadDTO.class); //UnidadDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<UnidadDTO> registrar(@Valid @RequestBody UnidadDTO dtoRequest) throws Exception{
		Unidad p = mapper.map(dtoRequest, Unidad.class);
		Unidad obj = service.registrar(p);
		UnidadDTO dtoResponse = mapper.map(obj, UnidadDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody UnidadDTO dtoRequest) throws Exception{
		Unidad p = mapper.map(dtoRequest, Unidad.class);
		Unidad obj = service.registrar(p);
		UnidadDTO dtoResponse = mapper.map(obj, UnidadDTO.class);
		//localhost:8080/Unidads/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdunidad()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<UnidadDTO> modificar(@RequestBody UnidadDTO dtoRequest) throws Exception {
		Unidad pac = service.listarPorId(dtoRequest.getIdunidad());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdunidad());	
		}
		
		Unidad p = mapper.map(dtoRequest, Unidad.class);
		 
		Unidad obj = service.modificar(p);
		
		UnidadDTO dtoResponse = mapper.map(obj, UnidadDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Unidad pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<UnidadDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Unidad obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		UnidadDTO dto = mapper.map(obj, UnidadDTO.class);
		
		EntityModel<UnidadDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Unidads/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Unidad-recurso1"));
		recurso.add(link2.withRel("Unidad-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<UnidadDTO>> listarPageable(Pageable page) throws Exception{
//		Page<UnidadDTO> Unidads = service.listarPageable(page).map(p -> mapper.map(p, UnidadDTO.class));
//		
//		return new ResponseEntity<>(Unidads, HttpStatus.OK);
//	}
}
