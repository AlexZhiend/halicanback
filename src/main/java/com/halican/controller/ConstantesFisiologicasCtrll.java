package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.ConstantesFisiologicas;
import com.halican.dto.ConstantesFisiologicasDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IConstantesFisiologicasService;


@RestController
@RequestMapping("/constantesfisiologicas")
public class ConstantesFisiologicasCtrll {

	@Autowired
	private IConstantesFisiologicasService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<ConstantesFisiologicasDTO>> listar() throws Exception{				
		List<ConstantesFisiologicasDTO> lista = service.listar().stream().map(p -> mapper.map(p, ConstantesFisiologicasDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ConstantesFisiologicasDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		ConstantesFisiologicasDTO dtoResponse;
		ConstantesFisiologicas obj = service.listarPorId(id); //ConstantesFisiologicas		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, ConstantesFisiologicasDTO.class); //ConstantesFisiologicasDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<ConstantesFisiologicasDTO> registrar(@Valid @RequestBody ConstantesFisiologicasDTO dtoRequest) throws Exception{
		ConstantesFisiologicas p = mapper.map(dtoRequest, ConstantesFisiologicas.class);
		ConstantesFisiologicas obj = service.registrar(p);
		ConstantesFisiologicasDTO dtoResponse = mapper.map(obj, ConstantesFisiologicasDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody ConstantesFisiologicasDTO dtoRequest) throws Exception{
		ConstantesFisiologicas p = mapper.map(dtoRequest, ConstantesFisiologicas.class);
		ConstantesFisiologicas obj = service.registrar(p);
		ConstantesFisiologicasDTO dtoResponse = mapper.map(obj, ConstantesFisiologicasDTO.class);
		//localhost:8080/ConstantesFisiologicass/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdconstantes()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<ConstantesFisiologicasDTO> modificar(@RequestBody ConstantesFisiologicasDTO dtoRequest) throws Exception {
		ConstantesFisiologicas pac = service.listarPorId(dtoRequest.getIdconstantes());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdconstantes());	
		}
		
		ConstantesFisiologicas p = mapper.map(dtoRequest, ConstantesFisiologicas.class);
		 
		ConstantesFisiologicas obj = service.modificar(p);
		
		ConstantesFisiologicasDTO dtoResponse = mapper.map(obj, ConstantesFisiologicasDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		ConstantesFisiologicas pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<ConstantesFisiologicasDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		ConstantesFisiologicas obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		ConstantesFisiologicasDTO dto = mapper.map(obj, ConstantesFisiologicasDTO.class);
		
		EntityModel<ConstantesFisiologicasDTO> recurso = EntityModel.of(dto);
		//localhost:8080/ConstantesFisiologicass/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("ConstantesFisiologicas-recurso1"));
		recurso.add(link2.withRel("ConstantesFisiologicas-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<ConstantesFisiologicasDTO>> listarPageable(Pageable page) throws Exception{
//		Page<ConstantesFisiologicasDTO> ConstantesFisiologicass = service.listarPageable(page).map(p -> mapper.map(p, ConstantesFisiologicasDTO.class));
//		
//		return new ResponseEntity<>(ConstantesFisiologicass, HttpStatus.OK);
//	}
}
