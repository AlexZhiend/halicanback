package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Reconsulta;
import com.halican.dto.ReconsultaDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IReconsultaService;


@RestController
@RequestMapping("/reconsulta")
public class ReconsultaCtrll {

	@Autowired
	private IReconsultaService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<ReconsultaDTO>> listar() throws Exception{				
		List<ReconsultaDTO> lista = service.listar().stream().map(p -> mapper.map(p, ReconsultaDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ReconsultaDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		ReconsultaDTO dtoResponse;
		Reconsulta obj = service.listarPorId(id); //Reconsulta		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, ReconsultaDTO.class); //ReconsultaDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<ReconsultaDTO> registrar(@Valid @RequestBody ReconsultaDTO dtoRequest) throws Exception{
		Reconsulta p = mapper.map(dtoRequest, Reconsulta.class);
		Reconsulta obj = service.registrar(p);
		ReconsultaDTO dtoResponse = mapper.map(obj, ReconsultaDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody ReconsultaDTO dtoRequest) throws Exception{
		Reconsulta p = mapper.map(dtoRequest, Reconsulta.class);
		Reconsulta obj = service.registrar(p);
		ReconsultaDTO dtoResponse = mapper.map(obj, ReconsultaDTO.class);
		//localhost:8080/Reconsultas/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdreconsulta()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<ReconsultaDTO> modificar(@RequestBody ReconsultaDTO dtoRequest) throws Exception {
		Reconsulta pac = service.listarPorId(dtoRequest.getIdreconsulta());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdreconsulta());	
		}
		
		Reconsulta p = mapper.map(dtoRequest, Reconsulta.class);
		 
		Reconsulta obj = service.modificar(p);
		
		ReconsultaDTO dtoResponse = mapper.map(obj, ReconsultaDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Reconsulta pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<ReconsultaDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Reconsulta obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		ReconsultaDTO dto = mapper.map(obj, ReconsultaDTO.class);
		
		EntityModel<ReconsultaDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Reconsultas/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Reconsulta-recurso1"));
		recurso.add(link2.withRel("Reconsulta-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<ReconsultaDTO>> listarPageable(Pageable page) throws Exception{
//		Page<ReconsultaDTO> Reconsultas = service.listarPageable(page).map(p -> mapper.map(p, ReconsultaDTO.class));
//		
//		return new ResponseEntity<>(Reconsultas, HttpStatus.OK);
//	}
	
}
