package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.RegistroDeBanio;
import com.halican.dto.RegistroDeBanioDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IRegistroBanioService;

@RestController
@RequestMapping("/registrobanio")
public class RegistroBanioCtrll {
	
	@Autowired
	private IRegistroBanioService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<RegistroDeBanioDTO>> listar() throws Exception{				
		List<RegistroDeBanioDTO> lista = service.listar().stream().map(p -> mapper.map(p, RegistroDeBanioDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<RegistroDeBanioDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		RegistroDeBanioDTO dtoResponse;
		RegistroDeBanio obj = service.listarPorId(id); //RegistroDeBanio		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, RegistroDeBanioDTO.class); //RegistroDeBanioDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<RegistroDeBanioDTO> registrar(@Valid @RequestBody RegistroDeBanioDTO dtoRequest) throws Exception{
		RegistroDeBanio p = mapper.map(dtoRequest, RegistroDeBanio.class);
		RegistroDeBanio obj = service.registrar(p);
		RegistroDeBanioDTO dtoResponse = mapper.map(obj, RegistroDeBanioDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody RegistroDeBanioDTO dtoRequest) throws Exception{
		RegistroDeBanio p = mapper.map(dtoRequest, RegistroDeBanio.class);
		RegistroDeBanio obj = service.registrar(p);
		RegistroDeBanioDTO dtoResponse = mapper.map(obj, RegistroDeBanioDTO.class);
		System.out.print(obj);
		//localhost:8080/RegistroDeBanios/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdregistrobanio()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<RegistroDeBanioDTO> modificar(@RequestBody RegistroDeBanioDTO dtoRequest) throws Exception {
		RegistroDeBanio pac = service.listarPorId(dtoRequest.getIdregistrobanio());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdregistrobanio());	
		}
		
		RegistroDeBanio p = mapper.map(dtoRequest, RegistroDeBanio.class);
		 
		RegistroDeBanio obj = service.modificar(p);
		
		RegistroDeBanioDTO dtoResponse = mapper.map(obj, RegistroDeBanioDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		RegistroDeBanio pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<RegistroDeBanioDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		RegistroDeBanio obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		RegistroDeBanioDTO dto = mapper.map(obj, RegistroDeBanioDTO.class);
		
		EntityModel<RegistroDeBanioDTO> recurso = EntityModel.of(dto);
		//localhost:8080/RegistroDeBanios/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("RegistroDeBanio-recurso1"));
		recurso.add(link2.withRel("RegistroDeBanio-recurso2"));
		
		return recurso;
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<RegistroDeBanioDTO>> listarPageable(Pageable page) throws Exception{
		Page<RegistroDeBanioDTO> RegistroDeBanios = service.listarPageable(page).map(p -> mapper.map(p, RegistroDeBanioDTO.class));
		
		return new ResponseEntity<>(RegistroDeBanios, HttpStatus.OK);
	}
}
