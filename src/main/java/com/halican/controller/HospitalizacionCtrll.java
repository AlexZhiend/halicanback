package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Hospitalizacion;
import com.halican.dto.HospitalizacionDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IHospitalizacionService;

@RestController
@RequestMapping("/hospitalizacion")
public class HospitalizacionCtrll {
	@Autowired
	private IHospitalizacionService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<HospitalizacionDTO>> listar() throws Exception{				
		List<HospitalizacionDTO> lista = service.listar().stream().map(p -> mapper.map(p, HospitalizacionDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<HospitalizacionDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		HospitalizacionDTO dtoResponse;
		Hospitalizacion obj = service.listarPorId(id); //Hospitalizacion		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, HospitalizacionDTO.class); //HospitalizacionDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<HospitalizacionDTO> registrar(@Valid @RequestBody HospitalizacionDTO dtoRequest) throws Exception{
		Hospitalizacion p = mapper.map(dtoRequest, Hospitalizacion.class);
		Hospitalizacion obj = service.registrar(p);
		HospitalizacionDTO dtoResponse = mapper.map(obj, HospitalizacionDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody HospitalizacionDTO dtoRequest) throws Exception{
		Hospitalizacion p = mapper.map(dtoRequest, Hospitalizacion.class);
		Hospitalizacion obj = service.registrar(p);
		HospitalizacionDTO dtoResponse = mapper.map(obj, HospitalizacionDTO.class);
		//localhost:8080/Hospitalizacions/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdhospitalizacion()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<HospitalizacionDTO> modificar(@RequestBody HospitalizacionDTO dtoRequest) throws Exception {
		Hospitalizacion pac = service.listarPorId(dtoRequest.getIdhospitalizacion());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdhospitalizacion());	
		}
		
		Hospitalizacion p = mapper.map(dtoRequest, Hospitalizacion.class);
		 
		Hospitalizacion obj = service.modificar(p);
		
		HospitalizacionDTO dtoResponse = mapper.map(obj, HospitalizacionDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Hospitalizacion pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<HospitalizacionDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Hospitalizacion obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		HospitalizacionDTO dto = mapper.map(obj, HospitalizacionDTO.class);
		
		EntityModel<HospitalizacionDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Hospitalizacions/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Hospitalizacion-recurso1"));
		recurso.add(link2.withRel("Hospitalizacion-recurso2"));
		
		return recurso;
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<HospitalizacionDTO>> listarPageable(Pageable page) throws Exception{
		Page<HospitalizacionDTO> Hospitalizacions = service.listarPageable(page).map(p -> mapper.map(p, HospitalizacionDTO.class));
		
		return new ResponseEntity<>(Hospitalizacions, HttpStatus.OK);
	}
}
