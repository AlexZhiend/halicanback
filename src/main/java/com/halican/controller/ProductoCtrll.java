package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Producto;
import com.halican.dto.ProductoDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IProductoService;

@RestController
@RequestMapping("/producto")
public class ProductoCtrll {
	@Autowired
	private IProductoService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<ProductoDTO>> listar() throws Exception{				
		List<ProductoDTO> lista = service.listar().stream().map(p -> mapper.map(p, ProductoDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProductoDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		ProductoDTO dtoResponse;
		Producto obj = service.listarPorId(id); //Producto		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, ProductoDTO.class); //ProductoDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<ProductoDTO> registrar(@Valid @RequestBody ProductoDTO dtoRequest) throws Exception{
		Producto p = mapper.map(dtoRequest, Producto.class);
		Producto obj = service.registrar(p);
		ProductoDTO dtoResponse = mapper.map(obj, ProductoDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody ProductoDTO dtoRequest) throws Exception{
		Producto p = mapper.map(dtoRequest, Producto.class);
		Producto obj = service.registrar(p);
		ProductoDTO dtoResponse = mapper.map(obj, ProductoDTO.class);
		//localhost:8080/Productos/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdproducto()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<ProductoDTO> modificar(@RequestBody ProductoDTO dtoRequest) throws Exception {
		Producto pac = service.listarPorId(dtoRequest.getIdproducto());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdproducto());	
		}
		
		Producto p = mapper.map(dtoRequest, Producto.class);
		 
		Producto obj = service.modificar(p);
		
		ProductoDTO dtoResponse = mapper.map(obj, ProductoDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Producto pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<ProductoDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Producto obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		ProductoDTO dto = mapper.map(obj, ProductoDTO.class);
		
		EntityModel<ProductoDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Productos/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Producto-recurso1"));
		recurso.add(link2.withRel("Producto-recurso2"));
		
		return recurso;
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<ProductoDTO>> listarPageable(Pageable page) throws Exception{
		Page<ProductoDTO> Productos = service.listarPageable(page).map(p -> mapper.map(p, ProductoDTO.class));
		
		return new ResponseEntity<>(Productos, HttpStatus.OK);
	}
	
	@GetMapping("/buscarnombrecod/{nombreocod}")
	public ResponseEntity<List<ProductoDTO>> SearchProducto(@PathVariable("nombreocod") String nombreocod) throws Exception{
		List<ProductoDTO> lista = service.listar().stream().map(p -> mapper.map(p, ProductoDTO.class)).collect(Collectors.toList());
		List<ProductoDTO> dtoResponse;
		List<Producto> obj = service.SearchProducto(nombreocod); //Producto		
		if(obj.size() == 0) {
			throw new ModeloNotFoundException("PRODUCTO NO ENCONTRADO");
		}else {
			dtoResponse = service.SearchProducto(nombreocod).stream().map(p -> mapper.map(p, ProductoDTO.class)).collect(Collectors.toList());
		}
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	@GetMapping("/buscarsubcategoria/{subcategoria}")
	public ResponseEntity<List<ProductoDTO>> BuscarBySubcategoria(@PathVariable("subcategoria") String subcategoria) throws Exception{
		List<ProductoDTO> dtoResponse;
		List<Producto> obj = service.Searchsubcategoria(subcategoria); //Producto		
		if(obj.size() == 0) {
			throw new ModeloNotFoundException("PRODUCTO NO ENCONTRADO");
		}else {
			dtoResponse = service.Searchsubcategoria(subcategoria).stream().map(p -> mapper.map(p, ProductoDTO.class)).collect(Collectors.toList());
		}
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
}
