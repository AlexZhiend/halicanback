package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Empresa;
import com.halican.dto.EmpresaDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IEmpresaService;

@RestController
@RequestMapping("/empresa")
public class EmpresaCtrll {

	@Autowired
	private IEmpresaService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<EmpresaDTO>> listar() throws Exception{				
		List<EmpresaDTO> lista = service.listar().stream().map(p -> mapper.map(p, EmpresaDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<EmpresaDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		EmpresaDTO dtoResponse;
		Empresa obj = service.listarPorId(id); //Empresa		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, EmpresaDTO.class); //EmpresaDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<EmpresaDTO> registrar(@Valid @RequestBody EmpresaDTO dtoRequest) throws Exception{
		Empresa p = mapper.map(dtoRequest, Empresa.class);
		Empresa obj = service.registrar(p);
		EmpresaDTO dtoResponse = mapper.map(obj, EmpresaDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody EmpresaDTO dtoRequest) throws Exception{
		Empresa p = mapper.map(dtoRequest, Empresa.class);
		Empresa obj = service.registrar(p);
		EmpresaDTO dtoResponse = mapper.map(obj, EmpresaDTO.class);
		//localhost:8080/Empresas/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdempresa()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<EmpresaDTO> modificar(@RequestBody EmpresaDTO dtoRequest) throws Exception {
		Empresa pac = service.listarPorId(dtoRequest.getIdempresa());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdempresa());	
		}
		
		Empresa p = mapper.map(dtoRequest, Empresa.class);
		 
		Empresa obj = service.modificar(p);
		
		EmpresaDTO dtoResponse = mapper.map(obj, EmpresaDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Empresa pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<EmpresaDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Empresa obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		EmpresaDTO dto = mapper.map(obj, EmpresaDTO.class);
		
		EntityModel<EmpresaDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Empresas/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Empresa-recurso1"));
		recurso.add(link2.withRel("Empresa-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<EmpresaDTO>> listarPageable(Pageable page) throws Exception{
//		Page<EmpresaDTO> Empresas = service.listarPageable(page).map(p -> mapper.map(p, EmpresaDTO.class));
//		
//		return new ResponseEntity<>(Empresas, HttpStatus.OK);
//	}
}
