package com.halican.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Comprobante;
import com.halican.Model.Serie;
import com.halican.dto.ComprobanteDTO;
import com.halican.dto.SerieDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IComprobanteService;
import com.halican.service.ISerieService;
import com.halican.Numero_a_letra;


@RestController
@RequestMapping("/comprobante")
public class ComprobanteCtrll {
	
	@Autowired
	private IComprobanteService service;
	
	@Autowired
	private ISerieService serieservice;
	
	@Autowired
	private ModelMapper mapper;
		
	@GetMapping
	public ResponseEntity<List<ComprobanteDTO>> listar() throws Exception{				
		List<ComprobanteDTO> lista = service.listar().stream().map(p -> mapper.map(p, ComprobanteDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ComprobanteDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		ComprobanteDTO dtoResponse;
		Comprobante obj = service.listarPorId(id); //Comprobante		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, ComprobanteDTO.class); //ComprobanteDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<ComprobanteDTO> registrar(@Valid @RequestBody ComprobanteDTO dtoRequest) throws Exception{
		Comprobante p = mapper.map(dtoRequest, Comprobante.class);
		Comprobante obj = service.registrar(p);
		ComprobanteDTO dtoResponse = mapper.map(obj, ComprobanteDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody ComprobanteDTO dtoRequest) throws Exception{
		Comprobante p = mapper.map(dtoRequest, Comprobante.class);

		Numero_a_letra NumLetra = new Numero_a_letra();
		String numero = String.format("%.2f", p.getTotal());
		p.setNumeroenletra(NumLetra.Convertir(numero,true));
		
		
//		Serie serie = new Serie();
//		serie= serieservice.BuscarCorrelativo(p.getTipocomprobante().getCodigotipocomprobante(), p.getSerie().getIdserie());
//		Integer nuevocorrelativo = serie.getCorrelativo()+1;
//		p.setCorrelativo(nuevocorrelativo);
//		serie.setCorrelativo(nuevocorrelativo);		
//		serieservice.modificar(serie);
					
		Comprobante obj = service.RegistrarComprobanteDetalle(p);
		ComprobanteDTO dtoResponse = mapper.map(obj, ComprobanteDTO.class);
		//localhost:8080/Comprobantes/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdcomprobante()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<ComprobanteDTO> modificar(@RequestBody ComprobanteDTO dtoRequest) throws Exception {
		Comprobante pac = service.listarPorId(dtoRequest.getIdcomprobante());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdcomprobante());	
		}
		
		Comprobante p = mapper.map(dtoRequest, Comprobante.class);
		 
		Comprobante obj = service.ActualizarComprobanteDetalle(p);
		
		ComprobanteDTO dtoResponse = mapper.map(obj, ComprobanteDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Comprobante pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<ComprobanteDTO>> listarPageable(Pageable page) throws Exception{
//		Page<ComprobanteDTO> Comprobantes = service.listarPageable(page).map(p -> mapper.map(p, ComprobanteDTO.class));
//		
//		return new ResponseEntity<>(Comprobantes, HttpStatus.OK);
//	}

	@GetMapping(value = "/ticketpdf/{correlativo}/{idserie}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteComprobante(@PathVariable("correlativo") Integer correlativo,@PathVariable("idserie") Integer idserie) throws Exception {
		byte[] data = null;
		data = service.generarReporteComprobanteU(correlativo, idserie);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
	@GetMapping(value= "/comprobantebyfechas/{fechainicio}/{fechafin}")
	public ResponseEntity<List<ComprobanteDTO>> comprobanteByfechas(@PathVariable("fechainicio") String fechainicio, @PathVariable("fechafin") String fechafin ) throws Exception{				
		List<ComprobanteDTO> lista = service.listarComprobanteByFechas(fechainicio,fechafin).stream().map(p -> mapper.map(p, ComprobanteDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/reportecaja/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteComprobante(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) throws Exception {
		byte[] data = null;
		data = service.reporteCajaPorFecha(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
}
