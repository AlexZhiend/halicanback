package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Rol;
import com.halican.dto.RolDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IRolService;

@RestController
@RequestMapping("/rol")
public class RolCtrll {
	
	@Autowired
	private IRolService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<RolDTO>> listar() throws Exception{				
		List<RolDTO> lista = service.listar().stream().map(p -> mapper.map(p, RolDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<RolDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		RolDTO dtoResponse;
		Rol obj = service.listarPorId(id); //Rol		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, RolDTO.class); //RolDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<RolDTO> registrar(@Valid @RequestBody RolDTO dtoRequest) throws Exception{
		Rol p = mapper.map(dtoRequest, Rol.class);
		Rol obj = service.registrar(p);
		RolDTO dtoResponse = mapper.map(obj, RolDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody RolDTO dtoRequest) throws Exception{
		Rol p = mapper.map(dtoRequest, Rol.class);
		Rol obj = service.registrar(p);
		RolDTO dtoResponse = mapper.map(obj, RolDTO.class);
		//localhost:8080/Rols/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdRol()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<RolDTO> modificar(@RequestBody RolDTO dtoRequest) throws Exception {
		Rol pac = service.listarPorId(dtoRequest.getIdRol());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdRol());	
		}
		
		Rol p = mapper.map(dtoRequest, Rol.class);
		 
		Rol obj = service.modificar(p);
		
		RolDTO dtoResponse = mapper.map(obj, RolDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Rol pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<RolDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Rol obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		RolDTO dto = mapper.map(obj, RolDTO.class);
		
		EntityModel<RolDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Rols/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Rol-recurso1"));
		recurso.add(link2.withRel("Rol-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<RolDTO>> listarPageable(Pageable page) throws Exception{
//		Page<RolDTO> Rols = service.listarPageable(page).map(p -> mapper.map(p, RolDTO.class));
//		
//		return new ResponseEntity<>(Rols, HttpStatus.OK);
//	}
}
