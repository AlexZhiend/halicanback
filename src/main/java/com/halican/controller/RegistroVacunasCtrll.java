package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.RegistroVacunas;
import com.halican.dto.RegistroVacunasDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IRegistroVacunaService;


@RestController
@RequestMapping("/registrovacunas")
public class RegistroVacunasCtrll {
	
	@Autowired
	private IRegistroVacunaService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<RegistroVacunasDTO>> listar() throws Exception{				
		List<RegistroVacunasDTO> lista = service.listar().stream().map(p -> mapper.map(p, RegistroVacunasDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<RegistroVacunasDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		RegistroVacunasDTO dtoResponse;
		RegistroVacunas obj = service.listarPorId(id); //RegistroVacunas		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, RegistroVacunasDTO.class); //RegistroVacunasDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<RegistroVacunasDTO> registrar(@Valid @RequestBody RegistroVacunasDTO dtoRequest) throws Exception{
		RegistroVacunas p = mapper.map(dtoRequest, RegistroVacunas.class);
		RegistroVacunas obj = service.registrar(p);
		RegistroVacunasDTO dtoResponse = mapper.map(obj, RegistroVacunasDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody RegistroVacunasDTO dtoRequest) throws Exception{
		RegistroVacunas p = mapper.map(dtoRequest, RegistroVacunas.class);
		RegistroVacunas obj = service.registrar(p);
		RegistroVacunasDTO dtoResponse = mapper.map(obj, RegistroVacunasDTO.class);
		//localhost:8080/RegistroVacunass/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdregistrovacuna()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<RegistroVacunasDTO> modificar(@RequestBody RegistroVacunasDTO dtoRequest) throws Exception {
		RegistroVacunas pac = service.listarPorId(dtoRequest.getIdregistrovacuna());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdregistrovacuna());	
		}
		
		RegistroVacunas p = mapper.map(dtoRequest, RegistroVacunas.class);
		 
		RegistroVacunas obj = service.modificar(p);
		
		RegistroVacunasDTO dtoResponse = mapper.map(obj, RegistroVacunasDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		RegistroVacunas pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<RegistroVacunasDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		RegistroVacunas obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		RegistroVacunasDTO dto = mapper.map(obj, RegistroVacunasDTO.class);
		
		EntityModel<RegistroVacunasDTO> recurso = EntityModel.of(dto);
		//localhost:8080/RegistroVacunass/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("RegistroVacunas-recurso1"));
		recurso.add(link2.withRel("RegistroVacunas-recurso2"));
		
		return recurso;
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<RegistroVacunasDTO>> listarPageable(Pageable page) throws Exception{
		Page<RegistroVacunasDTO> RegistroVacunass = service.listarPageable(page).map(p -> mapper.map(p, RegistroVacunasDTO.class));
		
		return new ResponseEntity<>(RegistroVacunass, HttpStatus.OK);
	}
}
