package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Moneda;
import com.halican.dto.MonedaDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IMonedaService;

@RestController
@RequestMapping("/moneda")
public class MonedaCtrll {

	@Autowired
	private IMonedaService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<MonedaDTO>> listar() throws Exception{				
		List<MonedaDTO> lista = service.listar().stream().map(p -> mapper.map(p, MonedaDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<MonedaDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		MonedaDTO dtoResponse;
		Moneda obj = service.listarPorId(id); //Moneda		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, MonedaDTO.class); //MonedaDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<MonedaDTO> registrar(@Valid @RequestBody MonedaDTO dtoRequest) throws Exception{
		Moneda p = mapper.map(dtoRequest, Moneda.class);
		Moneda obj = service.registrar(p);
		MonedaDTO dtoResponse = mapper.map(obj, MonedaDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody MonedaDTO dtoRequest) throws Exception{
		Moneda p = mapper.map(dtoRequest, Moneda.class);
		Moneda obj = service.registrar(p);
		MonedaDTO dtoResponse = mapper.map(obj, MonedaDTO.class);
		//localhost:8080/Monedas/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdmoneda()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<MonedaDTO> modificar(@RequestBody MonedaDTO dtoRequest) throws Exception {
		Moneda pac = service.listarPorId(dtoRequest.getIdmoneda());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdmoneda());	
		}
		
		Moneda p = mapper.map(dtoRequest, Moneda.class);
		 
		Moneda obj = service.modificar(p);
		
		MonedaDTO dtoResponse = mapper.map(obj, MonedaDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Moneda pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<MonedaDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Moneda obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		MonedaDTO dto = mapper.map(obj, MonedaDTO.class);
		
		EntityModel<MonedaDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Monedas/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Moneda-recurso1"));
		recurso.add(link2.withRel("Moneda-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<MonedaDTO>> listarPageable(Pageable page) throws Exception{
//		Page<MonedaDTO> Monedas = service.listarPageable(page).map(p -> mapper.map(p, MonedaDTO.class));
//		
//		return new ResponseEntity<>(Monedas, HttpStatus.OK);
//	}
}
