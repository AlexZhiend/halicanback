package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Proveedor;
import com.halican.dto.ProveedorDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IProveedorService;

@RestController
@RequestMapping("/proveedor")
public class ProveedorCtrll {
	
	@Autowired
	private IProveedorService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<ProveedorDTO>> listar() throws Exception{				
		List<ProveedorDTO> lista = service.listar().stream().map(p -> mapper.map(p, ProveedorDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProveedorDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		ProveedorDTO dtoResponse;
		Proveedor obj = service.listarPorId(id); //Proveedor		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, ProveedorDTO.class); //ProveedorDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<ProveedorDTO> registrar(@Valid @RequestBody ProveedorDTO dtoRequest) throws Exception{
		Proveedor p = mapper.map(dtoRequest, Proveedor.class);
		Proveedor obj = service.registrar(p);
		ProveedorDTO dtoResponse = mapper.map(obj, ProveedorDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody ProveedorDTO dtoRequest) throws Exception{
		Proveedor p = mapper.map(dtoRequest, Proveedor.class);
		Proveedor obj = service.registrar(p);
		ProveedorDTO dtoResponse = mapper.map(obj, ProveedorDTO.class);
		//localhost:8080/Proveedors/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdproveedor()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<ProveedorDTO> modificar(@RequestBody ProveedorDTO dtoRequest) throws Exception {
		Proveedor pac = service.listarPorId(dtoRequest.getIdproveedor());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdproveedor());	
		}
		
		Proveedor p = mapper.map(dtoRequest, Proveedor.class);
		 
		Proveedor obj = service.modificar(p);
		
		ProveedorDTO dtoResponse = mapper.map(obj, ProveedorDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Proveedor pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<ProveedorDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Proveedor obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		ProveedorDTO dto = mapper.map(obj, ProveedorDTO.class);
		
		EntityModel<ProveedorDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Proveedors/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Proveedor-recurso1"));
		recurso.add(link2.withRel("Proveedor-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<ProveedorDTO>> listarPageable(Pageable page) throws Exception{
//		Page<ProveedorDTO> Proveedors = service.listarPageable(page).map(p -> mapper.map(p, ProveedorDTO.class));
//		
//		return new ResponseEntity<>(Proveedors, HttpStatus.OK);
//	}
}
