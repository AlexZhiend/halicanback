package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.ExamenMedico;
import com.halican.dto.ExamenMedicoDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IExamenMedicoService;

@RestController
@RequestMapping("/examenmedico")
public class ExamenMedicoCtrll {

	@Autowired
	private IExamenMedicoService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<ExamenMedicoDTO>> listar() throws Exception{				
		List<ExamenMedicoDTO> lista = service.listar().stream().map(p -> mapper.map(p, ExamenMedicoDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ExamenMedicoDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		ExamenMedicoDTO dtoResponse;
		ExamenMedico obj = service.listarPorId(id); //ExamenMedico		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, ExamenMedicoDTO.class); //ExamenMedicoDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<ExamenMedicoDTO> registrar(@Valid @RequestBody ExamenMedicoDTO dtoRequest) throws Exception{
		ExamenMedico p = mapper.map(dtoRequest, ExamenMedico.class);
		ExamenMedico obj = service.registrar(p);
		ExamenMedicoDTO dtoResponse = mapper.map(obj, ExamenMedicoDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody ExamenMedicoDTO dtoRequest) throws Exception{
		ExamenMedico p = mapper.map(dtoRequest, ExamenMedico.class);
		ExamenMedico obj = service.registrar(p);
		ExamenMedicoDTO dtoResponse = mapper.map(obj, ExamenMedicoDTO.class);
		//localhost:8080/ExamenMedicos/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdexamen()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<ExamenMedicoDTO> modificar(@RequestBody ExamenMedicoDTO dtoRequest) throws Exception {
		ExamenMedico pac = service.listarPorId(dtoRequest.getIdexamen());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdexamen());	
		}
		
		ExamenMedico p = mapper.map(dtoRequest, ExamenMedico.class);
		 
		ExamenMedico obj = service.modificar(p);
		
		ExamenMedicoDTO dtoResponse = mapper.map(obj, ExamenMedicoDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		ExamenMedico pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<ExamenMedicoDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		ExamenMedico obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		ExamenMedicoDTO dto = mapper.map(obj, ExamenMedicoDTO.class);
		
		EntityModel<ExamenMedicoDTO> recurso = EntityModel.of(dto);
		//localhost:8080/ExamenMedicos/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("ExamenMedico-recurso1"));
		recurso.add(link2.withRel("ExamenMedico-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<ExamenMedicoDTO>> listarPageable(Pageable page) throws Exception{
//		Page<ExamenMedicoDTO> ExamenMedicos = service.listarPageable(page).map(p -> mapper.map(p, ExamenMedicoDTO.class));
//		
//		return new ResponseEntity<>(ExamenMedicos, HttpStatus.OK);
//	}
}
