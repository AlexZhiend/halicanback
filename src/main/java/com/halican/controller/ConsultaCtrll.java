package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Consulta;
import com.halican.dto.ConsultaDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IConsultaService;

@RestController
@RequestMapping("/consulta")
public class ConsultaCtrll {

	@Autowired
	private IConsultaService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<com.halican.dto.ConsultaDTO>> listar() throws Exception{				
		List<ConsultaDTO> lista = service.listar().stream().map(p -> mapper.map(p, ConsultaDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ConsultaDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		ConsultaDTO dtoResponse;
		Consulta obj = service.listarPorId(id); //Consulta		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, ConsultaDTO.class); //ConsultaDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<ConsultaDTO> registrar(@Valid @RequestBody ConsultaDTO dtoRequest) throws Exception{
		Consulta p = mapper.map(dtoRequest, Consulta.class);
		Consulta obj = service.registrar(p);
		ConsultaDTO dtoResponse = mapper.map(obj, ConsultaDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody ConsultaDTO dtoRequest) throws Exception{
		Consulta p = mapper.map(dtoRequest, Consulta.class);
		Consulta obj = service.RegistrarConsultaDetalle(p);
		ConsultaDTO dtoResponse = mapper.map(obj, ConsultaDTO.class);
		//localhost:8080/Consultas/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdconsulta()).toUri();
		System.out.print(p.getFecha());
		System.out.print(" ---- ");
		System.out.print(obj.getFecha());
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<ConsultaDTO> modificar(@RequestBody ConsultaDTO dtoRequest) throws Exception {
		Consulta pac = service.listarPorId(dtoRequest.getIdconsulta());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdconsulta());	
		}
		
		Consulta p = mapper.map(dtoRequest, Consulta.class);
		 
		Consulta obj = service.ActualizarConsultaDetalle(p);
		
		ConsultaDTO dtoResponse = mapper.map(obj, ConsultaDTO.class);
		System.out.print(p.getFecha());
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Consulta pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<ConsultaDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Consulta obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		ConsultaDTO dto = mapper.map(obj, ConsultaDTO.class);
		
		EntityModel<ConsultaDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Consultas/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Consulta-recurso1"));
		recurso.add(link2.withRel("Consulta-recurso2"));
		
		return recurso;
	}
	
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<ConsultaDTO>> listarPageable(Pageable page) throws Exception{
		Page<ConsultaDTO> Consultas = service.ListarPgeable(page).map(p -> mapper.map(p, ConsultaDTO.class));
		
		return new ResponseEntity<>(Consultas, HttpStatus.OK);
	}
	
}
