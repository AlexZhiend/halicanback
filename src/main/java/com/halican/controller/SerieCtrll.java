package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Serie;
import com.halican.dto.SerieDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.ISerieService;

@RestController
@RequestMapping("/serie")
public class SerieCtrll {
	
	@Autowired
	private ISerieService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<SerieDTO>> listar() throws Exception{				
		List<SerieDTO> lista = service.listar().stream().map(p -> mapper.map(p, SerieDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<SerieDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		SerieDTO dtoResponse;
		Serie obj = service.listarPorId(id); //Serie		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, SerieDTO.class); //SerieDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<SerieDTO> registrar(@Valid @RequestBody SerieDTO dtoRequest) throws Exception{
		Serie p = mapper.map(dtoRequest, Serie.class);
		Serie obj = service.registrar(p);
		SerieDTO dtoResponse = mapper.map(obj, SerieDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody SerieDTO dtoRequest) throws Exception{
		Serie p = mapper.map(dtoRequest, Serie.class);
		Serie obj = service.registrar(p);
		SerieDTO dtoResponse = mapper.map(obj, SerieDTO.class);
		//localhost:8080/Series/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdserie()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<SerieDTO> modificar(@RequestBody SerieDTO dtoRequest) throws Exception {
		Serie pac = service.listarPorId(dtoRequest.getIdserie());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdserie());	
		}
		
		Serie p = mapper.map(dtoRequest, Serie.class);
		 
		Serie obj = service.modificar(p);
		
		SerieDTO dtoResponse = mapper.map(obj, SerieDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Serie pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<SerieDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Serie obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		SerieDTO dto = mapper.map(obj, SerieDTO.class);
		
		EntityModel<SerieDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Series/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Serie-recurso1"));
		recurso.add(link2.withRel("Serie-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<SerieDTO>> listarPageable(Pageable page) throws Exception{
//		Page<SerieDTO> Series = service.listarPageable(page).map(p -> mapper.map(p, SerieDTO.class));
//		
//		return new ResponseEntity<>(Series, HttpStatus.OK);
//	}
	
	
	@GetMapping("/buscarcorrelativo/{codigotipocomprobante}/{idserie}")
	public ResponseEntity<SerieDTO> UltimoCorrelativo(@PathVariable("codigotipocomprobante") String codigotipocomprobante,@PathVariable("idserie") Integer idserie) throws Exception{
		
		SerieDTO dtoResponse;
		Serie obj = service.BuscarCorrelativo(codigotipocomprobante,idserie); //Serie		

		if(obj == null) {
			throw new ModeloNotFoundException("CORRELATIVO NO ENCONTRADO " + obj.getCorrelativo());
		}else {
			dtoResponse = mapper.map(obj, SerieDTO.class); //SerieDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	
	@GetMapping("/buscarserietipo/{idtipocomprobante}")
	public ResponseEntity<List<SerieDTO>> listar(@PathVariable("idtipocomprobante") Integer idtipocomprobante) throws Exception{				
		List<SerieDTO> lista = service.BuscarSerieTipo(idtipocomprobante).stream().map(p -> mapper.map(p, SerieDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
}
