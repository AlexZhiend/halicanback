package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Cuota;
import com.halican.dto.CuotaDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.ICuotaService;

@RestController
@RequestMapping("/cuota")
public class CuotaCtrll {
	
	@Autowired
	private ICuotaService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<CuotaDTO>> listar() throws Exception{				
		List<CuotaDTO> lista = service.listar().stream().map(p -> mapper.map(p, CuotaDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<CuotaDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		CuotaDTO dtoResponse;
		Cuota obj = service.listarPorId(id); //Cuota		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, CuotaDTO.class); //CuotaDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<CuotaDTO> registrar(@Valid @RequestBody CuotaDTO dtoRequest) throws Exception{
		Cuota p = mapper.map(dtoRequest, Cuota.class);
		Cuota obj = service.registrar(p);
		CuotaDTO dtoResponse = mapper.map(obj, CuotaDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody CuotaDTO dtoRequest) throws Exception{
		Cuota p = mapper.map(dtoRequest, Cuota.class);
		Cuota obj = service.registrar(p);
		CuotaDTO dtoResponse = mapper.map(obj, CuotaDTO.class);
		//localhost:8080/Cuotas/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdcuota()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<CuotaDTO> modificar(@RequestBody CuotaDTO dtoRequest) throws Exception {
		Cuota pac = service.listarPorId(dtoRequest.getIdcuota());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdcuota());	
		}
		
		Cuota p = mapper.map(dtoRequest, Cuota.class);
		 
		Cuota obj = service.modificar(p);
		
		CuotaDTO dtoResponse = mapper.map(obj, CuotaDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Cuota pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<CuotaDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Cuota obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		CuotaDTO dto = mapper.map(obj, CuotaDTO.class);
		
		EntityModel<CuotaDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Cuotas/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Cuota-recurso1"));
		recurso.add(link2.withRel("Cuota-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<CuotaDTO>> listarPageable(Pageable page) throws Exception{
//		Page<CuotaDTO> Cuotas = service.listarPageable(page).map(p -> mapper.map(p, CuotaDTO.class));
//		
//		return new ResponseEntity<>(Cuotas, HttpStatus.OK);
//	}
}
