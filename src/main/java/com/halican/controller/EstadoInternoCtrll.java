package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.EstadoInterno;
import com.halican.dto.EstadoInternoDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IEstadoInternoService;

@RestController
@RequestMapping("/estadointerno")
public class EstadoInternoCtrll {
	@Autowired
	private IEstadoInternoService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<EstadoInternoDTO>> listar() throws Exception{				
		List<EstadoInternoDTO> lista = service.listar().stream().map(p -> mapper.map(p, EstadoInternoDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<EstadoInternoDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		EstadoInternoDTO dtoResponse;
		EstadoInterno obj = service.listarPorId(id); //EstadoInterno		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, EstadoInternoDTO.class); //EstadoInternoDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<EstadoInternoDTO> registrar(@Valid @RequestBody EstadoInternoDTO dtoRequest) throws Exception{
		EstadoInterno p = mapper.map(dtoRequest, EstadoInterno.class);
		EstadoInterno obj = service.registrar(p);
		EstadoInternoDTO dtoResponse = mapper.map(obj, EstadoInternoDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody EstadoInternoDTO dtoRequest) throws Exception{
		EstadoInterno p = mapper.map(dtoRequest, EstadoInterno.class);
		EstadoInterno obj = service.registrar(p);
		EstadoInternoDTO dtoResponse = mapper.map(obj, EstadoInternoDTO.class);
		//localhost:8080/EstadoInternos/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdestadointerno()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<EstadoInternoDTO> modificar(@RequestBody EstadoInternoDTO dtoRequest) throws Exception {
		EstadoInterno pac = service.listarPorId(dtoRequest.getIdestadointerno());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdestadointerno());	
		}
		
		EstadoInterno p = mapper.map(dtoRequest, EstadoInterno.class);
		 
		EstadoInterno obj = service.modificar(p);
		
		EstadoInternoDTO dtoResponse = mapper.map(obj, EstadoInternoDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		EstadoInterno pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<EstadoInternoDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		EstadoInterno obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		EstadoInternoDTO dto = mapper.map(obj, EstadoInternoDTO.class);
		
		EntityModel<EstadoInternoDTO> recurso = EntityModel.of(dto);
		//localhost:8080/EstadoInternos/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("EstadoInterno-recurso1"));
		recurso.add(link2.withRel("EstadoInterno-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<EstadoInternoDTO>> listarPageable(Pageable page) throws Exception{
//		Page<EstadoInternoDTO> EstadoInternos = service.listarPageable(page).map(p -> mapper.map(p, EstadoInternoDTO.class));
//		
//		return new ResponseEntity<>(EstadoInternos, HttpStatus.OK);
//	}
}
