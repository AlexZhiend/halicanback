package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.RegistroDesparasitacion;
import com.halican.dto.RegistroDesparasitacionDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.IRegistroDesparasitacionService;

@RestController
@RequestMapping("/registrodesparasitacion")
public class RegistroDesparasitacionCtrll {
	@Autowired
	private IRegistroDesparasitacionService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<RegistroDesparasitacionDTO>> listar() throws Exception{				
		List<RegistroDesparasitacionDTO> lista = service.listar().stream().map(p -> mapper.map(p, RegistroDesparasitacionDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<RegistroDesparasitacionDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		RegistroDesparasitacionDTO dtoResponse;
		RegistroDesparasitacion obj = service.listarPorId(id); //RegistroDesparasitacion		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, RegistroDesparasitacionDTO.class); //RegistroDesparasitacionDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<RegistroDesparasitacionDTO> registrar(@Valid @RequestBody RegistroDesparasitacionDTO dtoRequest) throws Exception{
		RegistroDesparasitacion p = mapper.map(dtoRequest, RegistroDesparasitacion.class);
		RegistroDesparasitacion obj = service.registrar(p);
		RegistroDesparasitacionDTO dtoResponse = mapper.map(obj, RegistroDesparasitacionDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody RegistroDesparasitacionDTO dtoRequest) throws Exception{
		RegistroDesparasitacion p = mapper.map(dtoRequest, RegistroDesparasitacion.class);
		RegistroDesparasitacion obj = service.registrar(p);
		RegistroDesparasitacionDTO dtoResponse = mapper.map(obj, RegistroDesparasitacionDTO.class);
		//localhost:8080/RegistroDesparasitacions/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdregdesparasitacion()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<RegistroDesparasitacionDTO> modificar(@RequestBody RegistroDesparasitacionDTO dtoRequest) throws Exception {
		RegistroDesparasitacion pac = service.listarPorId(dtoRequest.getIdregdesparasitacion());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdregdesparasitacion());	
		}
		
		RegistroDesparasitacion p = mapper.map(dtoRequest, RegistroDesparasitacion.class);
		 
		RegistroDesparasitacion obj = service.modificar(p);
		
		RegistroDesparasitacionDTO dtoResponse = mapper.map(obj, RegistroDesparasitacionDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		RegistroDesparasitacion pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<RegistroDesparasitacionDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		RegistroDesparasitacion obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		RegistroDesparasitacionDTO dto = mapper.map(obj, RegistroDesparasitacionDTO.class);
		
		EntityModel<RegistroDesparasitacionDTO> recurso = EntityModel.of(dto);
		//localhost:8080/RegistroDesparasitacions/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("RegistroDesparasitacion-recurso1"));
		recurso.add(link2.withRel("RegistroDesparasitacion-recurso2"));
		
		return recurso;
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<RegistroDesparasitacionDTO>> listarPageable(Pageable page) throws Exception{
		Page<RegistroDesparasitacionDTO> RegistroDesparasitacions = service.listarPageable(page).map(p -> mapper.map(p, RegistroDesparasitacionDTO.class));
		
		return new ResponseEntity<>(RegistroDesparasitacions, HttpStatus.OK);
	}
}
