package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.Tratamiento;
import com.halican.dto.TratamientoDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.ITratamientoService;

@RestController
@RequestMapping("/tratamiento")
public class TratamientoCtrll {

	@Autowired
	private ITratamientoService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<TratamientoDTO>> listar() throws Exception{				
		List<TratamientoDTO> lista = service.listar().stream().map(p -> mapper.map(p, TratamientoDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<TratamientoDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		TratamientoDTO dtoResponse;
		Tratamiento obj = service.listarPorId(id); //Tratamiento		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, TratamientoDTO.class); //TratamientoDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<TratamientoDTO> registrar(@Valid @RequestBody TratamientoDTO dtoRequest) throws Exception{
		Tratamiento p = mapper.map(dtoRequest, Tratamiento.class);
		Tratamiento obj = service.registrar(p);
		TratamientoDTO dtoResponse = mapper.map(obj, TratamientoDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody TratamientoDTO dtoRequest) throws Exception{
		Tratamiento p = mapper.map(dtoRequest, Tratamiento.class);
		Tratamiento obj = service.registrar(p);
		TratamientoDTO dtoResponse = mapper.map(obj, TratamientoDTO.class);
		//localhost:8080/Tratamientos/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdtratamiento()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<TratamientoDTO> modificar(@RequestBody TratamientoDTO dtoRequest) throws Exception {
		Tratamiento pac = service.listarPorId(dtoRequest.getIdtratamiento());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdtratamiento());	
		}
		
		Tratamiento p = mapper.map(dtoRequest, Tratamiento.class);
		 
		Tratamiento obj = service.modificar(p);
		
		TratamientoDTO dtoResponse = mapper.map(obj, TratamientoDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Tratamiento pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<TratamientoDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		Tratamiento obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		TratamientoDTO dto = mapper.map(obj, TratamientoDTO.class);
		
		EntityModel<TratamientoDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Tratamientos/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("Tratamiento-recurso1"));
		recurso.add(link2.withRel("Tratamiento-recurso2"));
		
		return recurso;
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<TratamientoDTO>> listarPageable(Pageable page) throws Exception{
		Page<TratamientoDTO> Tratamientos = service.listarPageable(page).map(p -> mapper.map(p, TratamientoDTO.class));
		
		return new ResponseEntity<>(Tratamientos, HttpStatus.OK);
	}
}
