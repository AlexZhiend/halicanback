package com.halican.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.halican.Model.TipoDocumento;
import com.halican.dto.TipoDocumentoDTO;
import com.halican.exception.ModeloNotFoundException;
import com.halican.service.ITipoDocumentoService;

@RestController
@RequestMapping("/tipodocumento")
public class TipoDocumentoCtrll {
	
	@Autowired
	private ITipoDocumentoService service;
	
	@Autowired
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<TipoDocumentoDTO>> listar() throws Exception{				
		List<TipoDocumentoDTO> lista = service.listar().stream().map(p -> mapper.map(p, TipoDocumentoDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<TipoDocumentoDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		TipoDocumentoDTO dtoResponse;
		TipoDocumento obj = service.listarPorId(id); //TipoDocumento		

		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, TipoDocumentoDTO.class); //TipoDocumentoDTO
		}
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK); 		
	}
	
	/*@PostMapping
	public ResponseEntity<TipoDocumentoDTO> registrar(@Valid @RequestBody TipoDocumentoDTO dtoRequest) throws Exception{
		TipoDocumento p = mapper.map(dtoRequest, TipoDocumento.class);
		TipoDocumento obj = service.registrar(p);
		TipoDocumentoDTO dtoResponse = mapper.map(obj, TipoDocumentoDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED); 		
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody TipoDocumentoDTO dtoRequest) throws Exception{
		TipoDocumento p = mapper.map(dtoRequest, TipoDocumento.class);
		TipoDocumento obj = service.registrar(p);
		TipoDocumentoDTO dtoResponse = mapper.map(obj, TipoDocumentoDTO.class);
		//localhost:8080/TipoDocumentos/1
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdtipodocumento()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<TipoDocumentoDTO> modificar(@RequestBody TipoDocumentoDTO dtoRequest) throws Exception {
		TipoDocumento pac = service.listarPorId(dtoRequest.getIdtipodocumento());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdtipodocumento());	
		}
		
		TipoDocumento p = mapper.map(dtoRequest, TipoDocumento.class);
		 
		TipoDocumento obj = service.modificar(p);
		
		TipoDocumentoDTO dtoResponse = mapper.map(obj, TipoDocumentoDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		TipoDocumento pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<TipoDocumentoDTO> listarHateoasPorId(@PathVariable("id") Integer id) throws Exception{
		TipoDocumento obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		TipoDocumentoDTO dto = mapper.map(obj, TipoDocumentoDTO.class);
		
		EntityModel<TipoDocumentoDTO> recurso = EntityModel.of(dto);
		//localhost:8080/TipoDocumentos/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		WebMvcLinkBuilder link2 = linkTo(methodOn(this.getClass()).listarHateoasPorId(id));
		recurso.add(link1.withRel("TipoDocumento-recurso1"));
		recurso.add(link2.withRel("TipoDocumento-recurso2"));
		
		return recurso;
	}
	
//	@GetMapping("/pageable")
//	public ResponseEntity<Page<TipoDocumentoDTO>> listarPageable(Pageable page) throws Exception{
//		Page<TipoDocumentoDTO> TipoDocumentos = service.listarPageable(page).map(p -> mapper.map(p, TipoDocumentoDTO.class));
//		
//		return new ResponseEntity<>(TipoDocumentos, HttpStatus.OK);
//	}
}
