package com.halican.repository;

import com.halican.Model.Usuario;

public interface IUsuarioRepo extends IGenericRepo<Usuario, Integer>  {

	//from usuario where username = ?
	//Derived Query
	Usuario findOneByUsername(String username);	
}
