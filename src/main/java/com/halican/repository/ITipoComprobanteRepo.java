package com.halican.repository;

import com.halican.Model.TipoComprobante;

public interface ITipoComprobanteRepo extends IGenericRepo<TipoComprobante, Integer>{
	
	TipoComprobante findBycodigotipocomprobante(String codigo);

}
