package com.halican.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.halican.Model.Producto;

public interface IProductoRepo extends IGenericRepo<Producto, Integer>{

	@Query("FROM Producto pr where upper(concat(pr.codigobarras,pr.nombre)) like CONCAT('%',upper(:nombreocod),'%') and pr.cantidad>0")
	List<Producto> SearchProducto(@Param("nombreocod") String nombreocod);

	@Query(value="select * from producto pr where pr.subcategoria = :subcategoria and pr.cantidad>0;", nativeQuery = true)
	List<Producto> BuscarSubcategoria(@Param("subcategoria") String subcategoria);
	
}
