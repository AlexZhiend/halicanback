package com.halican.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;

import com.halican.Model.Comprobante;
import com.halican.Model.Producto;
import com.halican.Reportes.CajaFechaReport;
import com.halican.Reportes.ComprobanteUnitReport;

public interface IComprobanteRepo extends IGenericRepo<Comprobante, Integer>{

	@Query(value = "select * from comprobante order by correlativo desc limit 1", nativeQuery = true)
	public Comprobante buscarultimo();
	
	
	@Query(nativeQuery = true, name = "ReporteComporbanteUnit")
	List<ComprobanteUnitReport> reporteComprobanteU(@Param("correlativo") int correlativo,@Param("idserie") int idserie);
	
	
	@Query(value = "select * from comprobante cp where cp.fechaemision between TO_DATE(:fechainicio , 'YYYY/MM/DD') and TO_DATE(:fechafin , 'YYYY/MM/DD') ;", nativeQuery = true)
	List<Comprobante> ComprobantePorFecha(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin);
	
	@Query(nativeQuery=true, name = "ReporteCajaPorFecha")
	List<CajaFechaReport> ReporteCajaPorFecha(@Param("fechainicio") String fechainicio, @Param("fechafin") String fechafin);
	
}
