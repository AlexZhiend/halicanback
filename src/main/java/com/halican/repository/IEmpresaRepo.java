package com.halican.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.halican.Model.Empresa;

public interface IEmpresaRepo extends IGenericRepo<Empresa, Integer>{

	@Query(value="select * from empresa order by idempresa desc limit 1", nativeQuery = true)
	public Empresa buscarultimo();
	
}
