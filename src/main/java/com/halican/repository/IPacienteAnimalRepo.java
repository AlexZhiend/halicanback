package com.halican.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.halican.Model.PacienteAnimal;


public interface IPacienteAnimalRepo extends IGenericRepo<PacienteAnimal, Integer>{
	
	@Query(value="select * from pacienteanimal pac \r\n"
			+ "inner join cliente cl on cl.idcliente = pac.idcliente\r\n"
			+ "where upper(concat(cl.nombresyapellidos,cl.dni)) like upper(CONCAT('%', :parametro ,'%'))", nativeQuery = true)
	List<PacienteAnimal> BuscarPorCliente(@Param("parametro") String parametro);
	
	@Query(value="select * from pacienteanimal pac \r\n"
			+ "inner join cliente cl on cl.idcliente = pac.idcliente\r\n"
			+ "where upper(pac.nombre) like upper(CONCAT('%', :parametro ,'%'))", nativeQuery = true)
	List<PacienteAnimal> BuscarPorNombre(@Param("parametro") String parametro);

	@Query(value="select * from pacienteanimal pac order by pac.numerohistoria desc limit 1", nativeQuery = true)
	PacienteAnimal UltimaHistoria();
	
}
