package com.halican.repository;

import com.halican.Model.PersonalVeterinario;

public interface IPersonalVeterinarioRepo extends IGenericRepo<PersonalVeterinario, Integer>{

}
