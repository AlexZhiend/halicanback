package com.halican.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.halican.Model.Serie;

public interface ISerieRepo extends IGenericRepo<Serie, Integer>{
	
	@Query(value = "select * from serie se inner join tipo_comprobante tc on se.idtipocomprobante = tc.idtipocomprobante \r\n"
			+ "where tc.codigotipocomprobante = :codigotipocomprobante and se.idserie= :idserie ;", nativeQuery = true)
	Serie BusquedaCorrelativo(@Param("codigotipocomprobante") String codigotipocomprobante, @Param("idserie") Integer idserie);
	

	@Query(value = "select * from serie se inner join tipo_comprobante tc on se.idtipocomprobante = tc.idtipocomprobante \r\n"
			+ "where tc.idtipocomprobante= :idtipocomprobante ;", nativeQuery = true)
	List<Serie> BusquedaserieTipo(@Param("idtipocomprobante") int idtipocomprobante);
	
}
