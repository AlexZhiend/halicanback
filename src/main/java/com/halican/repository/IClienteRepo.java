package com.halican.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.halican.Model.Cliente;

public interface IClienteRepo extends IGenericRepo<Cliente, Integer>{
	
	@Query("FROM Cliente cl where upper(concat(cl.dni,cl.nombresyapellidos)) like CONCAT('%',upper(:nombreodni),'%')")
	List<Cliente> SearchCliente(@Param("nombreodni") String nombreodni);

}
