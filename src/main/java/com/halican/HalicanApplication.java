package com.halican;

import java.time.ZonedDateTime;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HalicanApplication {

//    @PostConstruct
//    void started() {
//      TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
//      System.out.println(ZonedDateTime.now());
//    }

	public static void main(String[] args) {
		SpringApplication.run(HalicanApplication.class, args);
	}

}
