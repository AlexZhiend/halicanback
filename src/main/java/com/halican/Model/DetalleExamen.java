package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class DetalleExamen {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int iddetalleexamen;
	
	@Column
	private String estado;
	
	@Column
	private LocalDateTime fecha;
	
	@Column
	private LocalDateTime fechaentrega;
	
	@ManyToOne
	@JoinColumn(name = "idexamenmedico", nullable=false)
	private ExamenMedico examenmedico;
	
	@ManyToOne
	@JoinColumn(name="idcliente", nullable= false)
	private Cliente cliente;

	
	
	public int getIddetalleexamen() {
		return iddetalleexamen;
	}

	public void setIddetalleexamen(int iddetalleexamen) {
		this.iddetalleexamen = iddetalleexamen;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public LocalDateTime getFechaentrega() {
		return fechaentrega;
	}

	public void setFechaentrega(LocalDateTime fechaentrega) {
		this.fechaentrega = fechaentrega;
	}

	public ExamenMedico getExamenmedico() {
		return examenmedico;
	}

	public void setExamenmedico(ExamenMedico examenmedico) {
		this.examenmedico = examenmedico;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
}
