package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.Data;

@Entity
@Table(name = "pacienteanimal")
public class PacienteAnimal {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idpacienteanimal;
	
	@Column(name="numerohistoria", unique = true)
	private int numerohistoria;
	
	@Column(name="nombre", nullable=false)
	private String nombre;
	
	@Column
	private String raza;
	
	@Column
	private String especie;
	
	@Column
	private String color;
	
	@Column
	private String esterilizacion;
	
	@Column
	private String sexo;
	
	@Column(name="fechanacimiento")
	private LocalDate fechanacimiento;
	
	@ManyToOne
	@JoinColumn(name = "idcliente", nullable=false)
	private Cliente cliente;

	public int getIdpacienteanimal() {
		return idpacienteanimal;
	}

	public void setIdpacienteanimal(int idpacienteanimal) {
		this.idpacienteanimal = idpacienteanimal;
	}

	public int getNumerohistoria() {
		return numerohistoria;
	}

	public void setNumerohistoria(int numerohistoria) {
		this.numerohistoria = numerohistoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getEsterilizacion() {
		return esterilizacion;
	}

	public void setEsterilizacion(String esterilizacion) {
		this.esterilizacion = esterilizacion;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public LocalDate getFechanacimiento() {
		return fechanacimiento;
	}

	public void setFechanacimiento(LocalDate fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
}
