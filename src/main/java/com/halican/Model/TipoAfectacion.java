package com.halican.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class TipoAfectacion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idtipoafectacion;
	
	@Column
	private String descripcion;
	
	@Column
	private String letra;
	
	@Column
	private String codigo;
	
	@Column
	private String nombre;
	
	@Column
	private String tipo;

	public int getIdtipoafectacion() {
		return idtipoafectacion;
	}

	public void setIdtipoafectacion(int idtipoafectacion) {
		this.idtipoafectacion = idtipoafectacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getLetra() {
		return letra;
	}

	public void setLetra(String letra) {
		this.letra = letra;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
