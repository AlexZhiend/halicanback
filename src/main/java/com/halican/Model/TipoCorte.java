package com.halican.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class TipoCorte {
	
	@Id
	@GeneratedValue
	private int idtipocorte;

	@Column
	private String descripcion;

	public int getIdtipocorte() {
		return idtipocorte;
	}

	public void setIdtipocorte(int idtipocorte) {
		this.idtipocorte = idtipocorte;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
