package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class RegistroDeHospedaje {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private	int idregistrohospedaje;
	
	@Column
	private LocalDateTime fechaentrada;
	
	@Column
	private LocalDateTime fechasalida;
	
	@Column
	private String estadoanimal;
	
	@Column
	private String alimento;
	
	@Column
	private String observaciones;
	
	@ManyToOne
	@JoinColumn(name = "idpacienteanimal", nullable = false)
	private PacienteAnimal pacienteanimal;

	public int getIdregistrohospedaje() {
		return idregistrohospedaje;
	}

	public void setIdregistrohospedaje(int idregistrohospedaje) {
		this.idregistrohospedaje = idregistrohospedaje;
	}

	public LocalDateTime getFechaentrada() {
		return fechaentrada;
	}

	public void setFechaentrada(LocalDateTime fechaentrada) {
		this.fechaentrada = fechaentrada;
	}

	public LocalDateTime getFechasalida() {
		return fechasalida;
	}

	public void setFechasalida(LocalDateTime fechasalida) {
		this.fechasalida = fechasalida;
	}

	public String getEstadoanimal() {
		return estadoanimal;
	}

	public void setEstadoanimal(String estadoanimal) {
		this.estadoanimal = estadoanimal;
	}

	public String getAlimento() {
		return alimento;
	}

	public void setAlimento(String alimento) {
		this.alimento = alimento;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public PacienteAnimal getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimal pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}

	
	
}
