package com.halican.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class EstadoInterno {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idestadointerno;
	
	@Column
	private String descripcion;

	public int getIdestadointerno() {
		return idestadointerno;
	}

	public void setIdestadointerno(int idestadointerno) {
		this.idestadointerno = idestadointerno;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
