package com.halican.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class TipoComprobante {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idtipocomprobante;
		
	@Column
	private String nomenclatura;
	
	@Column
	private String codigotipocomprobante;

	public int getIdtipocomprobante() {
		return idtipocomprobante;
	}

	public void setIdtipocomprobante(int idtipocomprobante) {
		this.idtipocomprobante = idtipocomprobante;
	}

	public String getNomenclatura() {
		return nomenclatura;
	}

	public void setNomenclatura(String nomenclatura) {
		this.nomenclatura = nomenclatura;
	}

	public String getCodigotipocomprobante() {
		return codigotipocomprobante;
	}

	public void setCodigotipocomprobante(String codigotipocomprobante) {
		this.codigotipocomprobante = codigotipocomprobante;
	}
	
	
	
}
