package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="constantesfisiologicas")
public class ConstantesFisiologicas {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idconstantes;
	
	@Column
	private LocalDateTime fecha;
	
	@Column
	private String consciente;

	@Column
	private String hidratacion;
	
	@Column
	private String mucosas;
	
	@Column
	private double peso;
	
	@Column
	private String frecuenciacardiaca;
	
	@Column
	private String frecuenciarespiratoria;
	
	@Column
	private String pulso;
	
	@Column
	private String tllenadocapilar;
	
	@Column
	private String temperaturas;
	
	@Column
	private String descripcionexamen;

	@ManyToOne
	@JoinColumn(name = "idconsulta", nullable = false)
	private Consulta consulta;

	public int getIdconstantes() {
		return idconstantes;
	}

	public void setIdconstantes(int idconstantes) {
		this.idconstantes = idconstantes;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getConsciente() {
		return consciente;
	}

	public void setConsciente(String consciente) {
		this.consciente = consciente;
	}

	public String getHidratacion() {
		return hidratacion;
	}

	public void setHidratacion(String hidratacion) {
		this.hidratacion = hidratacion;
	}

	public String getMucosas() {
		return mucosas;
	}

	public void setMucosas(String mucosas) {
		this.mucosas = mucosas;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public String getFrecuenciacardiaca() {
		return frecuenciacardiaca;
	}

	public void setFrecuenciacardiaca(String frecuenciacardiaca) {
		this.frecuenciacardiaca = frecuenciacardiaca;
	}

	public String getFrecuenciarespiratoria() {
		return frecuenciarespiratoria;
	}

	public void setFrecuenciarespiratoria(String frecuenciarespiratoria) {
		this.frecuenciarespiratoria = frecuenciarespiratoria;
	}

	public String getPulso() {
		return pulso;
	}

	public void setPulso(String pulso) {
		this.pulso = pulso;
	}

	public String getTllenadocapilar() {
		return tllenadocapilar;
	}

	public void setTllenadocapilar(String tllenadocapilar) {
		this.tllenadocapilar = tllenadocapilar;
	}

	public String getTemperaturas() {
		return temperaturas;
	}

	public void setTemperaturas(String temperaturas) {
		this.temperaturas = temperaturas;
	}

	public String getDescripcionexamen() {
		return descripcionexamen;
	}

	public void setDescripcionexamen(String descripcionexamen) {
		this.descripcionexamen = descripcionexamen;
	}

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
	
	
}
