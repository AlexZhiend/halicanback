package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity
@Table
public class Hospitalizacion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idhospitalizacion;
	
	@Column
	private LocalDateTime fechaingreso;
	
	@Column
	private String horaingreso;
	
	@Column
	private String motivoingreso;
	
	@Column
	private String diagnostico;
	
	@Column
	private String observaciones;
	
	@ManyToOne
	@JoinColumn(name = "idpacienteanimal", nullable = false)
	private PacienteAnimal pacienteanimal;

	public int getIdhospitalizacion() {
		return idhospitalizacion;
	}

	public void setIdhospitalizacion(int idhospitalizacion) {
		this.idhospitalizacion = idhospitalizacion;
	}

	public LocalDateTime getFechaingreso() {
		return fechaingreso;
	}

	public void setFechaingreso(LocalDateTime fechaingreso) {
		this.fechaingreso = fechaingreso;
	}

	public String getHoraingreso() {
		return horaingreso;
	}

	public void setHoraingreso(String horaingreso) {
		this.horaingreso = horaingreso;
	}

	public String getMotivoingreso() {
		return motivoingreso;
	}

	public void setMotivoingreso(String motivoingreso) {
		this.motivoingreso = motivoingreso;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public PacienteAnimal getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimal pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}



		
	
}
