package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class RegistroVacunas {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idregistrovacuna;
	
	@Column
	private LocalDateTime fechaaplicacion;
	
	@Column
	private LocalDateTime fechasiguiente;
	
	@Column
	private double peso;
	
	@Column
	private double cantidad;
	
	@Column
	private String vacuna;
	
	@Column
	private String lote;
	
	@Column
	private String laboratorio;
	
	@Column
	private String observaciones;
	
	@ManyToOne
	@JoinColumn(name = "idpacienteanimal", nullable = false)
	private PacienteAnimal pacienteanimal;

	public int getIdregistrovacuna() {
		return idregistrovacuna;
	}

	public void setIdregistrovacuna(int idregistrovacuna) {
		this.idregistrovacuna = idregistrovacuna;
	}

	public LocalDateTime getFechaaplicacion() {
		return fechaaplicacion;
	}

	public void setFechaaplicacion(LocalDateTime fechaaplicacion) {
		this.fechaaplicacion = fechaaplicacion;
	}

	public LocalDateTime getFechasiguiente() {
		return fechasiguiente;
	}

	public void setFechasiguiente(LocalDateTime fechasiguiente) {
		this.fechasiguiente = fechasiguiente;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public String getVacuna() {
		return vacuna;
	}

	public void setVacuna(String vacuna) {
		this.vacuna = vacuna;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public PacienteAnimal getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimal pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}

	
}
