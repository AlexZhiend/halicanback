package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.Data;

@Entity
@Table
public class PersonalVeterinario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idpersonal;
	
	@Column(name="dnipersonal", unique=true,nullable=false)
	private String dnipersonal;

	@Column
	private String nombrespersonal;
	
	@Column
	private String direccion;

	@Column
	private String correopersonal; 

	@Column
	private String telefonopersonal;

	@Column
	private String areapersonal;

	@Column
	private LocalDateTime fechanacimiento;

	public int getIdpersonal() {
		return idpersonal;
	}

	public void setIdpersonal(int idpersonal) {
		this.idpersonal = idpersonal;
	}

	public String getDnipersonal() {
		return dnipersonal;
	}

	public void setDnipersonal(String dnipersonal) {
		this.dnipersonal = dnipersonal;
	}

	public String getNombrespersonal() {
		return nombrespersonal;
	}

	public void setNombrespersonal(String nombrespersonal) {
		this.nombrespersonal = nombrespersonal;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCorreopersonal() {
		return correopersonal;
	}

	public void setCorreopersonal(String correopersonal) {
		this.correopersonal = correopersonal;
	}

	public String getTelefonopersonal() {
		return telefonopersonal;
	}

	public void setTelefonopersonal(String telefonopersonal) {
		this.telefonopersonal = telefonopersonal;
	}

	public String getAreapersonal() {
		return areapersonal;
	}

	public void setAreapersonal(String areapersonal) {
		this.areapersonal = areapersonal;
	}

	public LocalDateTime getFechanacimiento() {
		return fechanacimiento;
	}

	public void setFechanacimiento(LocalDateTime fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}
	
	
}
