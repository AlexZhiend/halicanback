package com.halican.Model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class Consulta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idconsulta;
	
	@Column
	private LocalDateTime fecha;
	
	@Column
	private String motivo;
	
	@Column
	private String inicio;
	
	@Column
	private String signossintomas;
	
	@Column
	private String receta;
				
	@ManyToOne
	@JoinColumn(name = "idpacienteanimal", nullable = false)
	private PacienteAnimal pacienteanimal;
	
	@ManyToOne
	@JoinColumn(name = "idpersonal", nullable=false) 
	private PersonalVeterinario personalveterinario;
	
	@OneToMany(mappedBy = "consulta", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
			fetch=FetchType.LAZY, orphanRemoval = true)
	private List<ConstantesFisiologicas> constantesfisiologicas;

	public Integer getIdconsulta() {
		return idconsulta;
	}

	public void setIdconsulta(Integer idconsulta) {
		this.idconsulta = idconsulta;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getInicio() {
		return inicio;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public String getSignossintomas() {
		return signossintomas;
	}

	public void setSignossintomas(String signossintomas) {
		this.signossintomas = signossintomas;
	}

	public PacienteAnimal getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimal pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}

	public PersonalVeterinario getPersonalveterinario() {
		return personalveterinario;
	}

	public void setPersonalveterinario(PersonalVeterinario personalveterinario) {
		this.personalveterinario = personalveterinario;
	}

	public String getReceta() {
		return receta;
	}

	public void setReceta(String receta) {
		this.receta = receta;
	}

	public List<ConstantesFisiologicas> getConstantesfisiologicas() {
		return constantesfisiologicas;
	}

	public void setConstantesfisiologicas(List<ConstantesFisiologicas> constantesfisiologicas) {
		this.constantesfisiologicas = constantesfisiologicas;
	}
	
	
}
