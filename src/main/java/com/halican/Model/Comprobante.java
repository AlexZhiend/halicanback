package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.halican.Reportes.CajaFechaReport;
import com.halican.Reportes.ComprobanteUnitReport;

import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.ConstructorResult;
import javax.persistence.ColumnResult;



@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "ReporteComporbanteUnit",
	    classes = @ConstructorResult(
	            targetClass = ComprobanteUnitReport.class,
	            columns = {
	            		@ColumnResult(name = "ids"),
	                    @ColumnResult(name = "descripcion"),
	                    @ColumnResult(name = "cantidad"),
	                    @ColumnResult(name = "preciounitario"),
	                    @ColumnResult(name = "importetotal"),
	                    @ColumnResult(name = "item"),
	                    @ColumnResult(name = "razonsocial"),
	                    @ColumnResult(name = "ruc"),
	                    @ColumnResult(name = "direccion"),
	                    @ColumnResult(name = "telefono"),
	                    @ColumnResult(name = "sucursal"),
	                    @ColumnResult(name = "nomenclatura"),
	                    @ColumnResult(name = "serie"),
	                    @ColumnResult(name = "correlativo"),
	                    @ColumnResult(name = "fechaemision"),
	                    @ColumnResult(name = "descuento"),
	                    @ColumnResult(name = "opgravadas"),
	                    @ColumnResult(name = "opinafectas"),
	                    @ColumnResult(name = "opexoneradas"),
	                    @ColumnResult(name = "igv"),
	                    @ColumnResult(name = "total"),
	                    @ColumnResult(name = "numeroenletra"),
	                    @ColumnResult(name = "subtotal"),
	                    @ColumnResult(name = "formapago"),
	                    @ColumnResult(name = "nombresyapellidos"),
	                    @ColumnResult(name = "dni"),
	                    
	            }
	    	)
		),
	@SqlResultSetMapping(
		    name = "ReporteCajaPorFecha",
		    classes = @ConstructorResult(
		            targetClass = CajaFechaReport.class,
		            columns = {
		            		@ColumnResult(name = "idcomprobante"),
		                    @ColumnResult(name = "serienombre"),
		                    @ColumnResult(name = "correlativo"),
		                    @ColumnResult(name = "fechaemision"),
		                    @ColumnResult(name = "descuento"),
		                    @ColumnResult(name = "total"),
		                    @ColumnResult(name = "formapago"),
		                    @ColumnResult(name = "metododepago"),
		                    @ColumnResult(name = "nombresyapellidos"),
		                    @ColumnResult(name = "dni"),
		                    
		            }
		    	)
			),
	}
)


@NamedNativeQueries({
	@NamedNativeQuery(name = "ReporteComporbanteUnit",resultClass = ComprobanteUnitReport.class, 
			query = "select det.iddetallecomprobante as ids, det.descripcion , det.cantidad, det.preciounitario, det.importetotal,\r\n"
					+ "det.item, em.razonsocial, em.ruc, em.direccion, em.telefono, em.sucursal, tp.nomenclatura, se.serie,cp.correlativo,\r\n"
					+ "cp.fechaemision,cp.descuento,cp.opgravadas,cp.opinafectas,cp.opexoneradas,cp.igv,cp.total,cp.numeroenletra,\r\n"
					+ "cp.total+cp.descuento as subtotal, cp.formapago, cl.nombresyapellidos, cl.dni\r\n"
					+ "from comprobante cp \r\n"
					+ "inner join detallecomprobante det on det.idcomprobante = cp.idcomprobante\r\n"
					+ "inner join empresa em on em.idempresa = cp.idempresa\r\n"
					+ "inner join serie se on se.idserie = cp.idserie\r\n"
					+ "inner join tipo_comprobante tp on tp.idtipocomprobante = cp.idtipocomprobante\r\n"
					+ "inner join cliente cl on cl.idcliente = cp.idcliente\r\n"
					+ "where cp.correlativo= :correlativo and cp.idserie= :idserie"
			),
	@NamedNativeQuery(name = "ReporteCajaPorFecha",resultClass = CajaFechaReport.class, 
	query = "select cp.idcomprobante, cp.serienombre, cp.correlativo, cp.formapago, "
			+ "to_char(cp.fechaemision,'YYYY-MM-DD') as fechaemision, cp.total, cl.nombresyapellidos, cl.dni,\r\n"
			+ "cp.descuento, cp.metododepago from comprobante cp inner join cliente cl on cl.idcliente = cp.idcliente\r\n"
			+ "where cp.fechaemision between TO_DATE(:fechainicio , 'YYYY/MM/DD') and TO_DATE(:fechafin , 'YYYY/MM/DD') \r\n"
			+ "and cp.estadointerno='Pagado' order by cp.serienombre asc;"
	),

})




@Entity
@Table(name="comprobante")
public class Comprobante {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idcomprobante;
	
	@ManyToOne
	@JoinColumn(name="idempresa")
	private Empresa empresa;
	
	@OneToOne
	@JoinColumn(name="idtipocomprobante",nullable=false)
	private TipoComprobante tipocomprobante;
	
	@ManyToOne
	@JoinColumn(name="idserie")
	private Serie serie;
	
	@Column
	private String serienombre;
	
	@Column
	private int correlativo;
	
	@Column
	private String formapago;
	
	@Column
	private LocalDateTime fechaemision;
	
	@Column
	private LocalDateTime fechavencimiento;
	
	@ManyToOne
	@JoinColumn(name = "idmoneda")
	private Moneda moneda;
	
	@Column
	private double opgravadas;
	
	@Column
	private double opexoneradas;
	
	@Column
	private double opinafectas;
	
	@Column
	private double igv;
	
	@Column
	private double total;
	
	@ManyToOne
	@JoinColumn(name="idcliente",nullable = false)
	private Cliente cliente;
	
	@Column
	private String idtipocompref;
	
	@Column
	private String serieref;
	
	@Column
	private int correlativoref;
	
	@Column
	private String codmotivo;
	
	@Column
	private String nombrexml;
	
	@Column
	private double descuento;
	
	@Column
	private String estadointerno;
	
	@Column
	private String xmlbase64;
	
	@Column
	private String hash;
	
	@Column
	private String codigosunat;
	
	@Column
	private String mensajesunat;
	
	@Column
	private String estadosunat;
				
	@Column
	private String observaciones;
	
	@Column
	private String metododepago;
	
	@Column
	private String numeroenletra;
			
	@OneToMany(mappedBy = "comprobante", cascade = {CascadeType.ALL},
			fetch=FetchType.EAGER, orphanRemoval = true)
	private List<DetalleComprobante> detallecomprobante;

	public int getIdcomprobante() {
		return idcomprobante;
	}

	public void setIdcomprobante(int idcomprobante) {
		this.idcomprobante = idcomprobante;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public TipoComprobante getTipocomprobante() {
		return tipocomprobante;
	}

	public void setTipocomprobante(TipoComprobante tipocomprobante) {
		this.tipocomprobante = tipocomprobante;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public String getSerienombre() {
		return serienombre;
	}

	public void setSerienombre(String serienombre) {
		this.serienombre = serienombre;
	}

	public int getCorrelativo() {
		return correlativo;
	}

	public void setCorrelativo(int correlativo) {
		this.correlativo = correlativo;
	}

	public String getFormapago() {
		return formapago;
	}

	public void setFormapago(String formapago) {
		this.formapago = formapago;
	}

	public LocalDateTime getFechaemision() {
		return fechaemision;
	}

	public void setFechaemision(LocalDateTime fechaemision) {
		this.fechaemision = fechaemision;
	}

	public LocalDateTime getFechavencimiento() {
		return fechavencimiento;
	}

	public void setFechavencimiento(LocalDateTime fechavencimiento) {
		this.fechavencimiento = fechavencimiento;
	}

	public Moneda getMoneda() {
		return moneda;
	}

	public void setMoneda(Moneda moneda) {
		this.moneda = moneda;
	}

	public double getOpgravadas() {
		return opgravadas;
	}

	public void setOpgravadas(double opgravadas) {
		this.opgravadas = opgravadas;
	}

	public double getOpexoneradas() {
		return opexoneradas;
	}

	public void setOpexoneradas(double opexoneradas) {
		this.opexoneradas = opexoneradas;
	}

	public double getOpinafectas() {
		return opinafectas;
	}

	public void setOpinafectas(double opinafectas) {
		this.opinafectas = opinafectas;
	}

	public double getIgv() {
		return igv;
	}

	public void setIgv(double igv) {
		this.igv = igv;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getIdtipocompref() {
		return idtipocompref;
	}

	public void setIdtipocompref(String idtipocompref) {
		this.idtipocompref = idtipocompref;
	}

	public String getSerieref() {
		return serieref;
	}

	public void setSerieref(String serieref) {
		this.serieref = serieref;
	}

	public int getCorrelativoref() {
		return correlativoref;
	}

	public void setCorrelativoref(int correlativoref) {
		this.correlativoref = correlativoref;
	}

	public String getCodmotivo() {
		return codmotivo;
	}

	public void setCodmotivo(String codmotivo) {
		this.codmotivo = codmotivo;
	}

	public String getNombrexml() {
		return nombrexml;
	}

	public void setNombrexml(String nombrexml) {
		this.nombrexml = nombrexml;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public String getEstadointerno() {
		return estadointerno;
	}

	public void setEstadointerno(String estadointerno) {
		this.estadointerno = estadointerno;
	}

	public String getXmlbase64() {
		return xmlbase64;
	}

	public void setXmlbase64(String xmlbase64) {
		this.xmlbase64 = xmlbase64;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getCodigosunat() {
		return codigosunat;
	}

	public void setCodigosunat(String codigosunat) {
		this.codigosunat = codigosunat;
	}

	public String getMensajesunat() {
		return mensajesunat;
	}

	public void setMensajesunat(String mensajesunat) {
		this.mensajesunat = mensajesunat;
	}

	public String getEstadosunat() {
		return estadosunat;
	}

	public void setEstadosunat(String estadosunat) {
		this.estadosunat = estadosunat;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public List<DetalleComprobante> getDetallecomprobante() {
		return detallecomprobante;
	}

	public void setDetallecomprobante(List<DetalleComprobante> detallecomprobante) {
		this.detallecomprobante = detallecomprobante;
	}

	public String getMetododepago() {
		return metododepago;
	}

	public void setMetododepago(String metododepago) {
		this.metododepago = metododepago;
	}

	public String getNumeroenletra() {
		return numeroenletra;
	}

	public void setNumeroenletra(String numeroenletra) {
		this.numeroenletra = numeroenletra;
	}
	
	
	
}
