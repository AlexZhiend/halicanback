package com.halican.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "detallecomprobante")
public class DetalleComprobante {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int iddetallecomprobante;
	
	@Column
	private int cantidad;
	
	@ManyToOne
	@JoinColumn(name = "idcomprobante",nullable=false)
	private Comprobante comprobante;
		
	@Column
	private int item;
	
	@Column
	private String descripcion;
	
	@Column
	private double preciounitario;
	
	@Column
	private double igv;
	
	@Column
	private double porcentajeigv;
	
	@Column
	private double valortotal;
	
	@Column
	private double importetotal;
	
	@Column
	private double valorunitario;
	
	@Column
	private double descuentoitem;
	
	@ManyToOne
	@JoinColumn(name = "idproducto", nullable = false)
	private Producto producto;

	public int getIddetallecomprobante() {
		return iddetallecomprobante;
	}

	public void setIddetallecomprobante(int iddetallecomprobante) {
		this.iddetallecomprobante = iddetallecomprobante;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Comprobante getComprobante() {
		return comprobante;
	}

	public void setComprobante(Comprobante comprobante) {
		this.comprobante = comprobante;
	}

	public int getItem() {
		return item;
	}

	public void setItem(int item) {
		this.item = item;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPreciounitario() {
		return preciounitario;
	}

	public void setPreciounitario(double preciounitario) {
		this.preciounitario = preciounitario;
	}

	public double getIgv() {
		return igv;
	}

	public void setIgv(double igv) {
		this.igv = igv;
	}

	public double getPorcentajeigv() {
		return porcentajeigv;
	}

	public void setPorcentajeigv(double porcentajeigv) {
		this.porcentajeigv = porcentajeigv;
	}

	public double getValortotal() {
		return valortotal;
	}

	public void setValortotal(double valortotal) {
		this.valortotal = valortotal;
	}

	public double getImportetotal() {
		return importetotal;
	}

	public void setImportetotal(double importetotal) {
		this.importetotal = importetotal;
	}

	public double getValorunitario() {
		return valorunitario;
	}

	public void setValorunitario(double valorunitario) {
		this.valorunitario = valorunitario;
	}

	public double getDescuentoitem() {
		return descuentoitem;
	}

	public void setDescuentoitem(double descuentoitem) {
		this.descuentoitem = descuentoitem;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	
	
}
