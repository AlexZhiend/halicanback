package com.halican.Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Cirujia {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idcirujia;
	
	@Column
	private String intervencion;
	
	@Column
	private LocalDateTime fecha;
	
	@Column
	private String hora;
	
	@Column
	private String observaciones;
	
	@ManyToOne
	@JoinColumn(name = "idpacienteanimal")
	private PacienteAnimal pacienteanimal;

	public int getIdcirujia() {
		return idcirujia;
	}

	public void setIdcirujia(int idcirujia) {
		this.idcirujia = idcirujia;
	}

	public String getIntervencion() {
		return intervencion;
	}

	public void setIntervencion(String intervencion) {
		this.intervencion = intervencion;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public PacienteAnimal getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimal pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}
	
	
	
}
