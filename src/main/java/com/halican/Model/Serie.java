package com.halican.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class Serie {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idserie;
		
	@Column
	private String serie;
	
	@Column
	private int correlativo;
	
	@ManyToOne
	@JoinColumn(name = "idtipocomprobante", nullable = false)
	private TipoComprobante tipoComprobante;

	public int getIdserie() {
		return idserie;
	}

	public void setIdserie(int idserie) {
		this.idserie = idserie;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public int getCorrelativo() {
		return correlativo;
	}

	public void setCorrelativo(int correlativo) {
		this.correlativo = correlativo;
	}

	public TipoComprobante getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(TipoComprobante tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}


	
	
}
