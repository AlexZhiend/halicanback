package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
@Entity
@Table
public class Tratamiento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private int idtratamiento;
	
	@Column
	private String descripcion;
	
	@Column
	private LocalDateTime fechainicio;
	
	@Column
	private LocalDateTime fechafin;
	
	@Column
	private String estado;
	
	@Column
	private String observaciones;
	
	@ManyToOne
	@JoinColumn(name = "idpaciente",nullable=false)
	private PacienteAnimal pacienteanimal;

	public int getIdtratamiento() {
		return idtratamiento;
	}

	public void setIdtratamiento(int idtratamiento) {
		this.idtratamiento = idtratamiento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDateTime getFechainicio() {
		return fechainicio;
	}

	public void setFechainicio(LocalDateTime fechainicio) {
		this.fechainicio = fechainicio;
	}

	public LocalDateTime getFechafin() {
		return fechafin;
	}

	public void setFechafin(LocalDateTime fechafin) {
		this.fechafin = fechafin;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public PacienteAnimal getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimal pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	
}
