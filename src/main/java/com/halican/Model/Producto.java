package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idproducto;

	@Column
	private String nombre;
	
	@Column
	private String codigobarras;
	
	@Column
	private LocalDate fechavencimiento;
	
	@Column
	private int cantidad;
	
	@Column
	private int stockinicial;
	
	@Column
	private String presentacion;
	
	@Column
	private double precioingreso;
	
	@Column(name = "marcaproducto")
	private String marcaproducto;
	
	@Column(name = "loteproducto")	
	private String loteproducto;
	
	@Column
	private LocalDate fechaingreso;
	
	@Column 
	private double valorunitario;
	
	@ManyToOne
	@JoinColumn(name = "idtipoafectacion")
	private TipoAfectacion tipoafectacion;
		
	@ManyToOne
	@JoinColumn(name = "idunidad")
	private Unidad unidad;
	
	@ManyToOne
	@JoinColumn(name = "idproveedor")
	private Proveedor proveedor;
	
	@ManyToOne
	@JoinColumn(name = "idcategoriaproducto", nullable =false)
	private CategoriaProducto categoriaproducto;
	
	@Column
	private String subcategoria;

	public int getIdproducto() {
		return idproducto;
	}

	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigobarras() {
		return codigobarras;
	}

	public void setCodigobarras(String codigobarras) {
		this.codigobarras = codigobarras;
	}

	public LocalDate getFechavencimiento() {
		return fechavencimiento;
	}

	public void setFechavencimiento(LocalDate fechavencimiento) {
		this.fechavencimiento = fechavencimiento;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getStockinicial() {
		return stockinicial;
	}

	public void setStockinicial(int stockinicial) {
		this.stockinicial = stockinicial;
	}

	public String getPresentacion() {
		return presentacion;
	}

	public void setPresentacion(String presentacion) {
		this.presentacion = presentacion;
	}

	public double getPrecioingreso() {
		return precioingreso;
	}

	public void setPrecioingreso(double precioingreso) {
		this.precioingreso = precioingreso;
	}

	public String getMarcaproducto() {
		return marcaproducto;
	}

	public void setMarcaproducto(String marcaproducto) {
		this.marcaproducto = marcaproducto;
	}

	public String getLoteproducto() {
		return loteproducto;
	}

	public void setLoteproducto(String loteproducto) {
		this.loteproducto = loteproducto;
	}

	public LocalDate getFechaingreso() {
		return fechaingreso;
	}

	public void setFechaingreso(LocalDate fechaingreso) {
		this.fechaingreso = fechaingreso;
	}

	public double getValorunitario() {
		return valorunitario;
	}

	public void setValorunitario(double valorunitario) {
		this.valorunitario = valorunitario;
	}

	public TipoAfectacion getTipoafectacion() {
		return tipoafectacion;
	}

	public void setTipoafectacion(TipoAfectacion tipoafectacion) {
		this.tipoafectacion = tipoafectacion;
	}

	public Unidad getUnidad() {
		return unidad;
	}

	public void setUnidad(Unidad unidad) {
		this.unidad = unidad;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public CategoriaProducto getCategoriaproducto() {
		return categoriaproducto;
	}

	public void setCategoriaproducto(CategoriaProducto categoriaproducto) {
		this.categoriaproducto = categoriaproducto;
	}

	public String getSubcategoria() {
		return subcategoria;
	}

	public void setSubcategoria(String subcategoria) {
		this.subcategoria = subcategoria;
	}
		
	
}
