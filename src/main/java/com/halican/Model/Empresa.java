package com.halican.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Entity
@Table(name = "empresa")
public class Empresa {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int idempresa;
	
	@Valid
	@NotBlank
	@Column
	private String ruc;
	
	@Valid
	@NotBlank
	@Column
	private String razonsocial;
	
	@Valid
	@NotBlank
	@Column
	private String nombrecomercial;
	
	@Valid
	@NotBlank
	@Column
	private String pais;
	
	@Column(nullable = false)
	private String departamento;
	
	@Column
	private String provincia;
	
	@Column
	private String distrito;
	
	@Column
	private String ubigeo;
	
	@Column
	private String usuariosol;
	
	@Column
	private String clavesol;
	
	@Column
	private String telefono;
	
	@Column
	private String direccion;
	
	@Column
	private String codigoestablecimiento;
	
	@Column
	private String correo;
	
	@Column
	private String sucursal;
	
	@Column
	private String link;
	
	@Column
	private String croquis;

	public int getIdempresa() {
		return idempresa;
	}

	public void setIdempresa(int idempresa) {
		this.idempresa = idempresa;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazonsocial() {
		return razonsocial;
	}

	public void setRazonsocial(String razonsocial) {
		this.razonsocial = razonsocial;
	}

	public String getNombrecomercial() {
		return nombrecomercial;
	}

	public void setNombrecomercial(String nombrecomercial) {
		this.nombrecomercial = nombrecomercial;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	public String getUsuariosol() {
		return usuariosol;
	}

	public void setUsuariosol(String usuariosol) {
		this.usuariosol = usuariosol;
	}

	public String getClavesol() {
		return clavesol;
	}

	public void setClavesol(String clavesol) {
		this.clavesol = clavesol;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCodigoestablecimiento() {
		return codigoestablecimiento;
	}

	public void setCodigoestablecimiento(String codigoestablecimiento) {
		this.codigoestablecimiento = codigoestablecimiento;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getCroquis() {
		return croquis;
	}

	public void setCroquis(String croquis) {
		this.croquis = croquis;
	}
	
	
}
