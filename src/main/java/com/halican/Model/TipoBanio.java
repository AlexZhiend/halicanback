package com.halican.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class TipoBanio {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private	int idtipobanio;
	
	@Column
	private String descripcion;

	public int getIdtipobanio() {
		return idtipobanio;
	}

	public void setIdtipobanio(int idtipobanio) {
		this.idtipobanio = idtipobanio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	
}
