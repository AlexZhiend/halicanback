package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class RegistroDeBanio {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private	int idregistrobanio;
	
	@Column
	private LocalDateTime fecha;
	
	@Column
	private String estadovisual;
	
	@Column
	private String observaciones;
	
	@Column
	private String estado;
	
	@ManyToOne
	@JoinColumn(name = "idtipobanio")
	private TipoBanio tipobanio;
	
	@ManyToOne
	@JoinColumn(name = "idtipocorte")
	private TipoCorte tipocorte;
	
	@ManyToOne
	@JoinColumn(name = "idpacienteanimal", nullable = false)
	private PacienteAnimal pacienteanimal;

	public int getIdregistrobanio() {
		return idregistrobanio;
	}

	public void setIdregistrobanio(int idregistrobanio) {
		this.idregistrobanio = idregistrobanio;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getEstadovisual() {
		return estadovisual;
	}

	public void setEstadovisual(String estadovisual) {
		this.estadovisual = estadovisual;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public TipoBanio getTipobanio() {
		return tipobanio;
	}

	public void setTipobanio(TipoBanio tipobanio) {
		this.tipobanio = tipobanio;
	}

	public TipoCorte getTipocorte() {
		return tipocorte;
	}

	public void setTipocorte(TipoCorte tipocorte) {
		this.tipocorte = tipocorte;
	}

	public PacienteAnimal getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimal pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}



}
