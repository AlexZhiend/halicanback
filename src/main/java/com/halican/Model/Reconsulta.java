package com.halican.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
public class Reconsulta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private	int idreconsulta;
	
	@Column
	private String motivo;
	
	@Column
	private LocalDateTime fecha;
	
	@Column
	private String signossintomas;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "idconsulta", nullable = false)
	private Consulta consulta;

	public int getIdreconsulta() {
		return idreconsulta;
	}

	public void setIdreconsulta(int idreconsulta) {
		this.idreconsulta = idreconsulta;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getSignossintomas() {
		return signossintomas;
	}

	public void setSignossintomas(String signossintomas) {
		this.signossintomas = signossintomas;
	}

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	} 
	
	
}
