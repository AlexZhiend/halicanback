package com.halican;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

//Segunda Clase
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter{

	@Autowired
    private ResourceServerTokenServices tokenServices;
	
	
    @Value("${security.jwt.resource-ids}")
    private String resourceIds;
    
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(resourceIds).tokenServices(tokenServices);
    }
    
    @Override
    public void configure(HttpSecurity http) throws Exception {
                http
                .exceptionHandling().authenticationEntryPoint(new AuthException())
                .and()
                .requestMatchers()
                .and()
                .authorizeRequests()                  
                .antMatchers("/v2/api-docs/**" ).permitAll()
                .antMatchers("/v3/api-docs/**" ).permitAll()
                .antMatchers("/consulta/**" ).authenticated()
                .antMatchers("/categoriaproducto/**" ).authenticated()
                .antMatchers("/constantesfisiologicas/**" ).authenticated()
                .antMatchers("/cuota/**" ).authenticated()
                .antMatchers("/consulta/**" ).authenticated()
                .antMatchers("/empresa/**" ).authenticated()
                .antMatchers("/cirujia/**" ).authenticated()
                .antMatchers("/estadointerno/**" ).authenticated()
                .antMatchers("/detalleexamen/**" ).authenticated()
                .antMatchers("/examenmedico/**" ).authenticated()
                .antMatchers("/hospitalizacion/**" ).authenticated()
                .antMatchers("/moneda/**" ).authenticated()
                .antMatchers("/pacienteanimal/**" ).authenticated()
                .antMatchers("/personalveterinario/**" ).authenticated()
                .antMatchers("/producto/**" ).authenticated()
                .antMatchers("/proveedor/**" ).authenticated()
                .antMatchers("/receta/**" ).authenticated()
                .antMatchers("/reconsulta/**" ).authenticated()
                .antMatchers("/registrobanio/**" ).authenticated()
                .antMatchers("/registrodesparasitacion/**" ).authenticated()
                .antMatchers("/registrodehospedaje/**" ).authenticated()
                .antMatchers("/registrovacunas/**" ).authenticated()
                .antMatchers("/rol/**" ).authenticated()
                .antMatchers("/serie/**" ).authenticated()
                .antMatchers("/tipoafectacion/**" ).authenticated()
                .antMatchers("/tipobanio/**" ).authenticated()
                .antMatchers("/tipocomprobante/**" ).authenticated()
                .antMatchers("/tipocorte/**" ).authenticated()
                .antMatchers("/tipodocumento/**" ).authenticated()
                .antMatchers("/tratamiento/**" ).authenticated()
                .antMatchers("/unidad/**" ).authenticated()
                .antMatchers("/usuario/**" ).authenticated()                                    
                .antMatchers("/menus/**" ).authenticated()
                .antMatchers("/perfil/**" ).authenticated()
                .antMatchers("/tokens/anular/**" ).permitAll()
                .antMatchers("/tokens/**" ).authenticated()
                ;                
                
    }    

}
