package com.halican.Reportes;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ComprobanteUnitReport {
	
	@Id
	private int ids;
	
	private String descripcion;
	
	private int cantidad;
	
	private double preciounitario;
	
	private double importetotal;
	private int item;
	private String razonsocial;
	private String ruc;
	private String direccion;
	private String telefono;
	private String sucursal;
	private String nomenclatura;
	private String serie;
	private int correlativo;
	private LocalDateTime fechaemision;
	private double descuento;
	private double opgravadas;
	private double opinafectas;
	private double opexoneradas;
	private double igv;
	private double total;
	private String numeroenletra;
	private double subtotal;
	private String formapago;
	private String nombresyapellidos;
	private String dni;
	public int getIds() {
		return ids;
	}
	public void setIds(int ids) {
		this.ids = ids;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public double getPreciounitario() {
		return preciounitario;
	}
	public void setPreciounitario(double preciounitario) {
		this.preciounitario = preciounitario;
	}
	public double getImportetotal() {
		return importetotal;
	}
	public void setImportetotal(double importetotal) {
		this.importetotal = importetotal;
	}
	public int getItem() {
		return item;
	}
	public void setItem(int item) {
		this.item = item;
	}
	public String getRazonsocial() {
		return razonsocial;
	}
	public void setRazonsocial(String razonsocial) {
		this.razonsocial = razonsocial;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getNomenclatura() {
		return nomenclatura;
	}
	public void setNomenclatura(String nomenclatura) {
		this.nomenclatura = nomenclatura;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public int getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(int correlativo) {
		this.correlativo = correlativo;
	}
	public LocalDateTime getFechaemision() {
		return fechaemision;
	}
	public void setFechaemision(LocalDateTime fechaemision) {
		this.fechaemision = fechaemision;
	}
	public double getDescuento() {
		return descuento;
	}
	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}
	public double getOpgravadas() {
		return opgravadas;
	}
	public void setOpgravadas(double opgravadas) {
		this.opgravadas = opgravadas;
	}
	public double getOpinafectas() {
		return opinafectas;
	}
	public void setOpinafectas(double opinafectas) {
		this.opinafectas = opinafectas;
	}
	public double getOpexoneradas() {
		return opexoneradas;
	}
	public void setOpexoneradas(double opexoneradas) {
		this.opexoneradas = opexoneradas;
	}
	public double getIgv() {
		return igv;
	}
	public void setIgv(double igv) {
		this.igv = igv;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getNumeroenletra() {
		return numeroenletra;
	}
	public void setNumeroenletra(String numeroenletra) {
		this.numeroenletra = numeroenletra;
	}
	public double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}
	public String getFormapago() {
		return formapago;
	}
	public void setFormapago(String formapago) {
		this.formapago = formapago;
	}
	public String getNombresyapellidos() {
		return nombresyapellidos;
	}
	public void setNombresyapellidos(String nombresyapellidos) {
		this.nombresyapellidos = nombresyapellidos;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	
	
}
