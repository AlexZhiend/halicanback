package com.halican.Reportes;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CajaFechaReport {
	
	@Id
	private int idcomprobante;
	private String serienombre;
	private int correlativo;
	private String formapago;
	private String fechaemision;
	private double total;
	private String nombresyapellidos;
	private String dni;
	private double descuento;
	private String metododepago;
	
	public int getIdcomprobante() {
		return idcomprobante;
	}
	public void setIdcomprobante(int idcomprobante) {
		this.idcomprobante = idcomprobante;
	}
	public String getSerienombre() {
		return serienombre;
	}
	public void setSerienombre(String serienombre) {
		this.serienombre = serienombre;
	}
	public int getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(int correlativo) {
		this.correlativo = correlativo;
	}
	public String getFormapago() {
		return formapago;
	}
	public void setFormapago(String formapago) {
		this.formapago = formapago;
	}

	public double getTotal() {
		return total;
	}
	public String getFechaemision() {
		return fechaemision;
	}
	public void setFechaemision(String fechaemision) {
		this.fechaemision = fechaemision;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getNombresyapellidos() {
		return nombresyapellidos;
	}
	public void setNombresyapellidos(String nombresyapellidos) {
		this.nombresyapellidos = nombresyapellidos;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public double getDescuento() {
		return descuento;
	}
	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}
	public String getMetododepago() {
		return metododepago;
	}
	public void setMetododepago(String metododepago) {
		this.metododepago = metododepago;
	}

}
