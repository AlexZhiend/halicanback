package com.halican.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.RegistroDesparasitacion;

public interface IRegistroDesparasitacionService extends ICRUD<RegistroDesparasitacion, Integer>{

	Page<RegistroDesparasitacion> listarPageable(Pageable page);
}
