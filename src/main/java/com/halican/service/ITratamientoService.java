package com.halican.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.Tratamiento;

public interface ITratamientoService extends ICRUD<Tratamiento, Integer>{

	Page<Tratamiento> listarPageable(Pageable page);
	
}
