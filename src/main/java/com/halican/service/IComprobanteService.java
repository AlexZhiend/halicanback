package com.halican.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.Comprobante;
import com.halican.Model.Producto;

public interface IComprobanteService extends ICRUD<Comprobante, Integer>{

	Comprobante RegistrarComprobanteDetalle(Comprobante comprobante) throws Exception;
	
	Comprobante ActualizarComprobanteDetalle(Comprobante comprobante) throws Exception;
	
	Comprobante BuscarUltimo() throws Exception;
	
	byte[] generarReporteComprobanteU(int correlativo, int idserie) throws Exception;
	
	List<Comprobante> listarComprobanteByFechas(String fechainicio, String fechafin) throws Exception;
	
	byte[] reporteCajaPorFecha(String fechainicio, String fechafin) throws Exception;

}
