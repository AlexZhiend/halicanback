package com.halican.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.RegistroVacunas;

public interface IRegistroVacunaService extends ICRUD<RegistroVacunas, Integer>{
	
	Page<RegistroVacunas> listarPageable(Pageable page);

}
