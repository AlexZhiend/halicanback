package com.halican.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.Comprobante;
import com.halican.Model.Consulta;

public interface IConsultaService extends ICRUD<Consulta, Integer>{

	Page<Consulta> ListarPgeable(Pageable page);
	
	Consulta RegistrarConsultaDetalle(Consulta consulta) throws Exception;
	
	Consulta ActualizarConsultaDetalle(Consulta consulta) throws Exception;
	
}
