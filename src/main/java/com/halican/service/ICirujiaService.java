package com.halican.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.Cirujia;

public interface ICirujiaService extends ICRUD<Cirujia, Integer>{
	
	Page<Cirujia> listarPageable(Pageable page);

}
