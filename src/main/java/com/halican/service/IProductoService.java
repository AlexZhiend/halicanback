package com.halican.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.Producto;

public interface IProductoService extends ICRUD<Producto, Integer>{

	Page<Producto> listarPageable(Pageable page);
	
	List<Producto> SearchProducto(String nombreocod);
	
	List<Producto> Searchsubcategoria(String subcategoria);
	
}
