package com.halican.service;

import java.util.List;

import com.halican.Model.Menu;


public interface IMenuService extends ICRUD<Menu, Integer>{
	
	List<Menu> listarMenuPorUsuario(String nombre);

}
