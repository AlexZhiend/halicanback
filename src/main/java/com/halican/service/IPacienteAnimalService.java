package com.halican.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.PacienteAnimal;

public interface IPacienteAnimalService extends ICRUD<PacienteAnimal, Integer>{

	Page<PacienteAnimal> listarPageable(Pageable page);
	
	List<PacienteAnimal> BuscarPorCliente(String parametro);
	
	List<PacienteAnimal> BuscarPornombre(String parametro);
	
	PacienteAnimal BuscarUltimaHistoria();
	
}
