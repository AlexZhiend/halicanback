package com.halican.service;

import com.halican.Model.Empresa;

public interface IEmpresaService extends ICRUD<Empresa, Integer>{

	Empresa buscarultimo() throws Exception;
	
}
