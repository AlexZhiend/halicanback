package com.halican.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.RegistroDeBanio;

public interface IRegistroBanioService extends ICRUD<RegistroDeBanio, Integer>{
	
	Page<RegistroDeBanio> listarPageable(Pageable page);

}
