package com.halican.service;

import com.halican.Model.TipoComprobante;

public interface ITipoComprobanteService extends ICRUD<TipoComprobante, Integer>{

	TipoComprobante BuscarPorCodigo(String codigosunat);
	
}
