package com.halican.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.Hospitalizacion;

public interface IHospitalizacionService extends ICRUD<Hospitalizacion, Integer>{
	
	Page<Hospitalizacion> listarPageable(Pageable page);

}
