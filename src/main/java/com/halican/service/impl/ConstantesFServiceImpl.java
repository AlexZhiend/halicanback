package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.ConstantesFisiologicas;
import com.halican.repository.IConstantesFisiologicasRepo;
import com.halican.repository.IGenericRepo;
import com.halican.service.IConstantesFisiologicasService;

@Service
public class ConstantesFServiceImpl extends CRUDImpl<ConstantesFisiologicas, Integer> implements IConstantesFisiologicasService{
	
	@Autowired
	private IConstantesFisiologicasRepo repo;

	@Override
	protected IGenericRepo<ConstantesFisiologicas, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

}
