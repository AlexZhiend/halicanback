package com.halican.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.Producto;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IProductoRepo;
import com.halican.service.IProductoService;

@Service
public class ProductoServiceImpl extends CRUDImpl<Producto, Integer> implements IProductoService{
	
	@Autowired
	private IProductoRepo repo;

	@Override
	protected IGenericRepo<Producto, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Page<Producto> listarPageable(Pageable page) {
		// TODO Auto-generated method stub
		return repo.findAll(page);
	}

	@Override
	public List<Producto> SearchProducto(String nombreocod) {
		// TODO Auto-generated method stub
		return repo.SearchProducto(nombreocod);
	}

	@Override
	public List<Producto> Searchsubcategoria(String subcategoria) {
		// TODO Auto-generated method stub
		return repo.BuscarSubcategoria(subcategoria);
	}

}
