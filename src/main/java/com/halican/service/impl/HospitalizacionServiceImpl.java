package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.Hospitalizacion;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IHospitalizacionRepo;
import com.halican.service.IHospitalizacionService;

@Service
public class HospitalizacionServiceImpl extends CRUDImpl<Hospitalizacion, Integer> implements IHospitalizacionService{
	
	@Autowired
	private IHospitalizacionRepo repo;

	@Override
	protected IGenericRepo<Hospitalizacion, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Page<Hospitalizacion> listarPageable(Pageable page) {
		// TODO Auto-generated method stub
		return repo.findAll(page);
	}
}
