package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.TipoDocumento;
import com.halican.repository.IGenericRepo;
import com.halican.repository.ITipoDocumentoRepo;
import com.halican.service.ITipoDocumentoService;

@Service
public class TipoDocumentoServiceImpl extends CRUDImpl<TipoDocumento, Integer> implements ITipoDocumentoService{

	@Autowired
	private ITipoDocumentoRepo repo;
	
	@Override
	protected IGenericRepo<TipoDocumento, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	
}
