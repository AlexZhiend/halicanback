package com.halican.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.Cliente;
import com.halican.repository.IClienteRepo;
import com.halican.repository.IGenericRepo;
import com.halican.service.IClienteService;

@Service
public class ClienteServiceImpl extends CRUDImpl<Cliente, Integer> implements IClienteService{
	
	@Autowired
	private IClienteRepo repo;

	@Override
	protected IGenericRepo<Cliente, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Page<Cliente> listarPageable(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public List<Cliente> BuscarCliente(String nombreodni) {
		// TODO Auto-generated method stub
		return repo.SearchCliente(nombreodni);
	}


}
