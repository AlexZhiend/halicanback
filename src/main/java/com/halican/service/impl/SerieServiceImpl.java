package com.halican.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.Serie;
import com.halican.Model.TipoAfectacion;
import com.halican.repository.IGenericRepo;
import com.halican.repository.ISerieRepo;
import com.halican.repository.ITipoAfectacionRepo;
import com.halican.service.ISerieService;
import com.halican.service.ITipoAfectacionService;

@Service
public class SerieServiceImpl extends CRUDImpl<Serie, Integer> implements ISerieService{

	@Autowired
	private ISerieRepo repo;

	@Override
	protected IGenericRepo<Serie, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Serie BuscarCorrelativo(String codigotipocomprobante, Integer idserie) {
		// TODO Auto-generated method stub
		return repo.BusquedaCorrelativo(codigotipocomprobante, idserie);
	}

	@Override
	public List<Serie> BuscarSerieTipo(Integer idtipocomprobante) {
		// TODO Auto-generated method stub
		return repo.BusquedaserieTipo(idtipocomprobante);
	}
	

}
