package com.halican.service.impl;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.Comprobante;
import com.halican.repository.IComprobanteRepo;
import com.halican.repository.IGenericRepo;
import com.halican.service.IComprobanteService;


import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class ComprobanteServiceImpl extends CRUDImpl<Comprobante, Integer> implements IComprobanteService{
	
	@Autowired
	private IComprobanteRepo repo;

	@Override
	protected IGenericRepo<Comprobante, Integer> getRepo() {
		return repo;
	}

	
	@Override
	public Comprobante RegistrarComprobanteDetalle(Comprobante comprobante) throws Exception {	
		comprobante.getDetallecomprobante().forEach(x -> x.setComprobante(comprobante));

		return repo.save(comprobante);
	}
	
	@Override
	public Comprobante ActualizarComprobanteDetalle(Comprobante comprobante) throws Exception {	
		comprobante.getDetallecomprobante().forEach(x -> x.setComprobante(comprobante));

		return repo.save(comprobante);
	}
	
	

	@Override
	public Comprobante BuscarUltimo() throws Exception {
		return repo.buscarultimo();
	}


	@Override
	public byte[] generarReporteComprobanteU(int correlativo, int idserie) throws Exception {
		
		byte[] data = null;
		try {
			File file = new ClassPathResource("./Reportes/Ticket.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(repo.reporteComprobanteU(correlativo, idserie)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}


	@Override
	public List<Comprobante> listarComprobanteByFechas(String fechainicio, String fechafin) {
		// TODO Auto-generated method stub
		return repo.ComprobantePorFecha(fechainicio, fechafin);
	}


	@Override
	public byte[] reporteCajaPorFecha(String fechainicio, String fechafin) throws Exception {
		
		byte[] data = null;
		try {
			File file = new ClassPathResource("./Reportes/ReporteCaja.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(repo.ReporteCajaPorFecha(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}



	
}
