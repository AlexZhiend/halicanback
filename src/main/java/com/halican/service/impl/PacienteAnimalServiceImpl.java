package com.halican.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.PacienteAnimal;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IPacienteAnimalRepo;
import com.halican.service.IPacienteAnimalService;

@Service
public class PacienteAnimalServiceImpl extends CRUDImpl<PacienteAnimal, Integer> implements IPacienteAnimalService{
	
	@Autowired
	private IPacienteAnimalRepo repo;

	@Override
	protected IGenericRepo<PacienteAnimal, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Page<PacienteAnimal> listarPageable(Pageable page) {
		// TODO Auto-generated method stub
		return repo.findAll(page);
	}

	@Override
	public List<PacienteAnimal> BuscarPorCliente(String parametro) {
		// TODO Auto-generated method stub
		return repo.BuscarPorCliente(parametro);
	}

	@Override
	public List<PacienteAnimal> BuscarPornombre(String parametro) {
		// TODO Auto-generated method stub
		return repo.BuscarPorNombre(parametro);
	}

	@Override
	public PacienteAnimal BuscarUltimaHistoria() {
		// TODO Auto-generated method stub
		return repo.UltimaHistoria();
	}

}
