package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.ExamenMedico;
import com.halican.repository.IExamenMedicoRepo;
import com.halican.repository.IGenericRepo;
import com.halican.service.IExamenMedicoService;

@Service
public class ExamenMedicoServiceImpl extends CRUDImpl<ExamenMedico, Integer> implements IExamenMedicoService{
	
	@Autowired
	private IExamenMedicoRepo repo;

	@Override
	protected IGenericRepo<ExamenMedico, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}
}
