package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.RegistroVacunas;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IRegistroVacunasRepo;
import com.halican.service.IRegistroVacunaService;

@Service
public class RegistroVacunaServiceImpl extends CRUDImpl<RegistroVacunas, Integer> implements IRegistroVacunaService{

	@Autowired
	private IRegistroVacunasRepo repo;
	
	@Override
	protected IGenericRepo<RegistroVacunas, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Page<RegistroVacunas> listarPageable(Pageable page) {
		// TODO Auto-generated method stub
		return repo.findAll(page);
	}

}
