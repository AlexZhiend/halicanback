package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.halican.Model.Empresa;
import com.halican.repository.IEmpresaRepo;
import com.halican.repository.IGenericRepo;
import com.halican.service.IEmpresaService;

@Service
public class EmpresaServiceImpl extends CRUDImpl<Empresa, Integer> implements IEmpresaService{
	
	@Autowired
	private IEmpresaRepo repo;

	@Transactional
	@Override
	protected IGenericRepo<Empresa, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Empresa buscarultimo() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
