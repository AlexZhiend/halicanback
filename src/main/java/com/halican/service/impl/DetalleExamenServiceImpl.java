package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.DetalleExamen;
import com.halican.repository.IDetalleExamen;
import com.halican.repository.IGenericRepo;
import com.halican.service.IDetalleExamenService;

@Service
public class DetalleExamenServiceImpl extends CRUDImpl<DetalleExamen, Integer> implements IDetalleExamenService{

	@Autowired
	private IDetalleExamen repo;

	@Override
	protected IGenericRepo<DetalleExamen, Integer> getRepo() {
		return repo;
	}
	
	
}
