package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.Unidad;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IUnidadRepo;
import com.halican.service.IUnidadService;

@Service
public class UnidadServiceImpl extends CRUDImpl<Unidad, Integer> implements IUnidadService{

	@Autowired
	private IUnidadRepo repo;
	
	@Override
	protected IGenericRepo<Unidad, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

}
