package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.Serie;
import com.halican.Model.TipoAfectacion;
import com.halican.repository.IGenericRepo;
import com.halican.repository.ISerieRepo;
import com.halican.repository.ITipoAfectacionRepo;
import com.halican.service.ISerieService;
import com.halican.service.ITipoAfectacionService;

@Service
public class TipoAfectacionServiceImpl extends CRUDImpl<TipoAfectacion, Integer> implements ITipoAfectacionService{

	@Autowired
	private ITipoAfectacionRepo repo;

	@Override
	protected IGenericRepo<TipoAfectacion, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}
}
