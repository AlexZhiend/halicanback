package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.Cuota;
import com.halican.repository.ICuotaRepo;
import com.halican.repository.IGenericRepo;
import com.halican.service.ICuotaService;

@Service
public class CuotaServiceImpl extends CRUDImpl<Cuota, Integer> implements ICuotaService{
	
	@Autowired
	private ICuotaRepo repo;

	@Override
	protected IGenericRepo<Cuota, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}
}
