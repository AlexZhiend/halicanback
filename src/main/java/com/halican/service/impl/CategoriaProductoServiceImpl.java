package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.CategoriaProducto;
import com.halican.repository.ICategoriaProductoRepo;
import com.halican.repository.IGenericRepo;
import com.halican.service.ICategoriaProductoService;

@Service
public class CategoriaProductoServiceImpl extends CRUDImpl<CategoriaProducto, Integer> implements ICategoriaProductoService{
	
	@Autowired
	private ICategoriaProductoRepo repo;

	@Override
	protected IGenericRepo<CategoriaProducto, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

}
