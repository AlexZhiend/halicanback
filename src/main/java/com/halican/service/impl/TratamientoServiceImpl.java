package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.Tratamiento;
import com.halican.repository.IGenericRepo;
import com.halican.repository.ITratamientoRepo;
import com.halican.service.ITratamientoService;

@Service
public class TratamientoServiceImpl extends CRUDImpl< Tratamiento, Integer> implements ITratamientoService{

	@Autowired
	private ITratamientoRepo repo;
	
	@Override
	protected IGenericRepo<Tratamiento, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Page<Tratamiento> listarPageable(Pageable page) {
		// TODO Auto-generated method stub
		return repo.findAll(page);
	}

}
