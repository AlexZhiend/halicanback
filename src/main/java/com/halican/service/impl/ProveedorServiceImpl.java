package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.Proveedor;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IProveedorRepo;
import com.halican.service.IProveedorService;

@Service
public class ProveedorServiceImpl extends CRUDImpl<Proveedor, Integer> implements IProveedorService{
	
	@Autowired
	private IProveedorRepo repo;

	@Override
	protected IGenericRepo<Proveedor, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

}
