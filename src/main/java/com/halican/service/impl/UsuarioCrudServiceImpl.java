package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.Usuario;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IUsuarioRepo;
import com.halican.service.IUsuarioService;

@Service
public class UsuarioCrudServiceImpl extends CRUDImpl< Usuario, Integer> implements IUsuarioService{

	@Autowired
	private IUsuarioRepo repo;
	
	@Override
	protected IGenericRepo<Usuario, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

}
