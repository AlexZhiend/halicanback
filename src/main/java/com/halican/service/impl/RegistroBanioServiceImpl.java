package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.RegistroDeBanio;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IRegistroBanioRepo;
import com.halican.service.IRegistroBanioService;

@Service
public class RegistroBanioServiceImpl extends CRUDImpl<RegistroDeBanio, Integer> implements IRegistroBanioService{
	
	@Autowired
	private IRegistroBanioRepo repo;

	@Override
	protected IGenericRepo<RegistroDeBanio, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Page<RegistroDeBanio> listarPageable(Pageable page) {
		// TODO Auto-generated method stub
		return repo.findAll(page);
	}
}
