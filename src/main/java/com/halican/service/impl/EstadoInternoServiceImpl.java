package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.EstadoInterno;
import com.halican.repository.IEstadoInternoRepo;
import com.halican.repository.IGenericRepo;
import com.halican.service.IEstadoInternoService;

@Service
public class EstadoInternoServiceImpl extends CRUDImpl<EstadoInterno, Integer> implements IEstadoInternoService{
	
	@Autowired
	private IEstadoInternoRepo repo;

	@Override
	protected IGenericRepo<EstadoInterno, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}
}
