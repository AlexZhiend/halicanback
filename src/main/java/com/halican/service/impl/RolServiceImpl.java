package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.Rol;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IRolRepo;
import com.halican.service.IRolService;

@Service
public class RolServiceImpl extends CRUDImpl<Rol, Integer> implements IRolService{

	@Autowired
	private IRolRepo repo;

	@Override
	protected IGenericRepo<Rol, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}
	
}
