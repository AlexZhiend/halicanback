package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.TipoCorte;
import com.halican.repository.IGenericRepo;
import com.halican.repository.ITipoCorteRepo;
import com.halican.service.ITipoCorteService;

@Service
public class TipoCorteServiceImpl extends CRUDImpl<TipoCorte, Integer> implements ITipoCorteService{

	@Autowired
	private ITipoCorteRepo repo;
	
	@Override
	protected IGenericRepo<TipoCorte, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}
}
