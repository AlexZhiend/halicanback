package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.Reconsulta;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IReconsultaRepo;
import com.halican.service.IReconsultaService;

@Service
public class ReconsultaServiceImpl extends CRUDImpl<Reconsulta, Integer> implements IReconsultaService{
	
	@Autowired
	private IReconsultaRepo repo;

	@Override
	protected IGenericRepo<Reconsulta, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

}
