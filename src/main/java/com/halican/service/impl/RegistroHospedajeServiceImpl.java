package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.RegistroDeHospedaje;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IRegistroHospedajeRepo;
import com.halican.service.IRegistroHospedajeService;

@Service
public class RegistroHospedajeServiceImpl extends CRUDImpl<RegistroDeHospedaje, Integer> implements IRegistroHospedajeService{
	
	@Autowired
	private IRegistroHospedajeRepo repo;

	@Override
	protected IGenericRepo<RegistroDeHospedaje, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Page<RegistroDeHospedaje> listarPageable(Pageable page) {
		// TODO Auto-generated method stub
		return repo.findAll(page);
	}

}
