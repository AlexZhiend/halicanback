package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.PersonalVeterinario;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IPersonalVeterinarioRepo;
import com.halican.service.IPersonalVeterinarioService;

@Service
public class PersonalVeterinarioServiceImpl extends CRUDImpl<PersonalVeterinario, Integer> implements IPersonalVeterinarioService{
	
	@Autowired
	private IPersonalVeterinarioRepo repo;

	@Override
	protected IGenericRepo<PersonalVeterinario, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

}
