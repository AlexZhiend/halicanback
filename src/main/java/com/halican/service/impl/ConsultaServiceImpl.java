package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.Comprobante;
import com.halican.Model.Consulta;
import com.halican.repository.IConsultaRepo;
import com.halican.repository.IGenericRepo;
import com.halican.service.IConsultaService;

@Service
public class ConsultaServiceImpl extends CRUDImpl<Consulta, Integer> implements IConsultaService{
	
	@Autowired
	private IConsultaRepo repo;
	
	@Override
	public Consulta RegistrarConsultaDetalle(Consulta consulta) throws Exception {	
		consulta.getConstantesfisiologicas().forEach(x -> x.setConsulta(consulta));

		return repo.save(consulta);
	}
	
	@Override
	public Consulta ActualizarConsultaDetalle(Consulta consulta) throws Exception {	
		consulta.getConstantesfisiologicas().forEach(x -> x.setConsulta(consulta));

		return repo.save(consulta);
	}

	@Override
	protected IGenericRepo<Consulta, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Page<Consulta> ListarPgeable(Pageable page) {
		// TODO Auto-generated method stub
		return repo.findAll(page);
	}

}
