package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.Cirujia;
import com.halican.repository.ICirujiaRepo;
import com.halican.repository.IGenericRepo;
import com.halican.service.ICirujiaService;

@Service
public class CirujiaServiceImpl extends CRUDImpl<Cirujia, Integer> implements ICirujiaService{

	@Autowired
	private ICirujiaRepo repo;
	
	@Override
	public Page<Cirujia> listarPageable(Pageable page) {
		// TODO Auto-generated method stub
		return repo.findAll(page);
	}

	@Override
	protected IGenericRepo<Cirujia, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

}
