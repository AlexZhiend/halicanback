package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.halican.Model.RegistroDesparasitacion;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IRegistroDesparasitacionRepo;
import com.halican.service.IRegistroDesparasitacionService;

@Service
public class RegistroDesparasitacionServiceImpl extends CRUDImpl<RegistroDesparasitacion, Integer> implements IRegistroDesparasitacionService{
	
	@Autowired
	private IRegistroDesparasitacionRepo repo;

	@Override
	protected IGenericRepo<RegistroDesparasitacion, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public Page<RegistroDesparasitacion> listarPageable(Pageable page) {
		// TODO Auto-generated method stub
		return repo.findAll(page);
	}

}
