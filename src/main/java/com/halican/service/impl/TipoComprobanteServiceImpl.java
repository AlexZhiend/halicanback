package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.TipoComprobante;
import com.halican.repository.IGenericRepo;
import com.halican.repository.ITipoComprobanteRepo;
import com.halican.service.ITipoComprobanteService;

@Service
public class TipoComprobanteServiceImpl extends CRUDImpl<TipoComprobante, Integer> implements ITipoComprobanteService{

	@Autowired
	private ITipoComprobanteRepo repo;
	
	@Override
	protected IGenericRepo<TipoComprobante, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Override
	public TipoComprobante BuscarPorCodigo(String codigosunat) {
		// TODO Auto-generated method stub
		return repo.findBycodigotipocomprobante(codigosunat);
	}
}
