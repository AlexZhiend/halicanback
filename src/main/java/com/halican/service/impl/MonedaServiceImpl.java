package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.Moneda;
import com.halican.repository.IGenericRepo;
import com.halican.repository.IMonedaRepo;
import com.halican.service.IMonedaService;

@Service
public class MonedaServiceImpl extends CRUDImpl<Moneda, Integer> implements IMonedaService{
	
	@Autowired
	private IMonedaRepo repo;

	@Override
	protected IGenericRepo<Moneda, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}
}
