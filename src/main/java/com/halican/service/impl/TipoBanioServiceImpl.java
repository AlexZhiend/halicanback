package com.halican.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.halican.Model.TipoBanio;
import com.halican.repository.IGenericRepo;
import com.halican.repository.ITipoBanioRepo;
import com.halican.service.ITipoBanioService;


@Service
public class TipoBanioServiceImpl extends CRUDImpl<TipoBanio, Integer> implements ITipoBanioService{

	@Autowired
	private ITipoBanioRepo repo;
	
	@Override
	protected IGenericRepo<TipoBanio, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}
}
