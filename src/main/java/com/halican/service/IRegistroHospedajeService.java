package com.halican.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.RegistroDeHospedaje;

public interface IRegistroHospedajeService extends ICRUD<RegistroDeHospedaje, Integer>{

	Page<RegistroDeHospedaje> listarPageable(Pageable page);
}
