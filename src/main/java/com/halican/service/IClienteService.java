package com.halican.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.halican.Model.Cliente;

public interface IClienteService extends ICRUD<Cliente, Integer>{
	
	Page<Cliente> listarPageable(Pageable page);
	List<Cliente> BuscarCliente(String nombreodni);

}
