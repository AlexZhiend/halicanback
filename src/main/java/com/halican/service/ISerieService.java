package com.halican.service;

import java.util.List;

import com.halican.Model.Serie;

public interface ISerieService extends ICRUD<Serie, Integer>{

	
	Serie BuscarCorrelativo(String codigotipocomprobante,Integer idserie);
	List<Serie> BuscarSerieTipo(Integer idtipocomprobante);
	 
}
