package com.halican.dto;

import java.time.LocalDateTime;

public class RegistroVacunasDTO {

	private int idregistrovacuna;
	
	private LocalDateTime fechaaplicacion;
	
	private LocalDateTime fechasiguiente;
	
	private double peso;
	
	private double cantidad;
	
	private String vacuna;
	
	private String lote;
	
	private String laboratorio;
	
	private String observaciones;
	
	private PacienteAnimalDTO pacienteanimal;

	public int getIdregistrovacuna() {
		return idregistrovacuna;
	}

	public void setIdregistrovacuna(int idregistrovacuna) {
		this.idregistrovacuna = idregistrovacuna;
	}

	public LocalDateTime getFechaaplicacion() {
		return fechaaplicacion;
	}

	public void setFechaaplicacion(LocalDateTime fechaaplicacion) {
		this.fechaaplicacion = fechaaplicacion;
	}

	public LocalDateTime getFechasiguiente() {
		return fechasiguiente;
	}

	public void setFechasiguiente(LocalDateTime fechasiguiente) {
		this.fechasiguiente = fechasiguiente;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public String getVacuna() {
		return vacuna;
	}

	public void setVacuna(String vacuna) {
		this.vacuna = vacuna;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public PacienteAnimalDTO getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimalDTO pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}


	
	
}
