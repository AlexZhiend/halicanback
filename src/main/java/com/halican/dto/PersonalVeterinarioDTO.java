package com.halican.dto;

import java.time.LocalDateTime;

public class PersonalVeterinarioDTO {

	private int idpersonal;
	
	private String dnipersonal;

	private String nombrespersonal;
	
	private String direccion;

	private String correopersonal; 

	private String telefonopersonal;

	private String areapersonal;

	private LocalDateTime fechanacimiento;

	public int getIdpersonal() {
		return idpersonal;
	}

	public void setIdpersonal(int idpersonal) {
		this.idpersonal = idpersonal;
	}

	public String getDnipersonal() {
		return dnipersonal;
	}

	public void setDnipersonal(String dnipersonal) {
		this.dnipersonal = dnipersonal;
	}

	public String getNombrespersonal() {
		return nombrespersonal;
	}

	public void setNombrespersonal(String nombrespersonal) {
		this.nombrespersonal = nombrespersonal;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCorreopersonal() {
		return correopersonal;
	}

	public void setCorreopersonal(String correopersonal) {
		this.correopersonal = correopersonal;
	}

	public String getTelefonopersonal() {
		return telefonopersonal;
	}

	public void setTelefonopersonal(String telefonopersonal) {
		this.telefonopersonal = telefonopersonal;
	}

	public String getAreapersonal() {
		return areapersonal;
	}

	public void setAreapersonal(String areapersonal) {
		this.areapersonal = areapersonal;
	}

	public LocalDateTime getFechanacimiento() {
		return fechanacimiento;
	}

	public void setFechanacimiento(LocalDateTime fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}
	
	
	
}
