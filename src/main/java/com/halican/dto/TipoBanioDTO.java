package com.halican.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

public class TipoBanioDTO {

	private	int idtipobanio;
	
	private String descripcion;

	public int getIdtipobanio() {
		return idtipobanio;
	}

	public void setIdtipobanio(int idtipobanio) {
		this.idtipobanio = idtipobanio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}
