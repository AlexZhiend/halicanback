package com.halican.dto;

import java.time.LocalDateTime;
import java.util.List;

public class ComprobanteDTO {
	
	private int idcomprobante;
	
	private EmpresaDTO empresa;
	
	private TipoComprobanteDTO tipocomprobante;
	
	private SerieDTO serie;
	
	private String serienombre;
	
	private int correlativo;
	
	private String formapago;
	
	private LocalDateTime fechaemision;
	
	private LocalDateTime fechavencimiento;
	
	private MonedaDTO moneda;
	
	private double opgravadas;
	
	private double opexoneradas;
	
	private double opinafectas;
	
	private double igv;
	
	private double total;
	
	private ClienteDTO cliente;
	
	private String idtipocompref;
	
	private String serieref;
	
	private int correlativoref;
	
	private String codmotivo;
	
	private String nombrexml;
	
	private double descuento;
	
	private String estadointerno;
	
	private String xmlbase64;
	
	private String hash;
	
	private String codigosunat;
	
	private String mensajesunat;
	
	private String estadosunat;
				
	private String observaciones;
	
	private String metododepago;
	
	private String numeroenletra;
			
	private List<DetalleComprobanteDTO> detallecomprobante;

	public int getIdcomprobante() {
		return idcomprobante;
	}

	public void setIdcomprobante(int idcomprobante) {
		this.idcomprobante = idcomprobante;
	}

	public EmpresaDTO getEmpresa() {
		return empresa;
	}

	public void setEmpresa(EmpresaDTO empresa) {
		this.empresa = empresa;
	}

	public TipoComprobanteDTO getTipocomprobante() {
		return tipocomprobante;
	}

	public void setTipocomprobante(TipoComprobanteDTO tipocomprobante) {
		this.tipocomprobante = tipocomprobante;
	}

	public SerieDTO getSerie() {
		return serie;
	}

	public void setSerie(SerieDTO serie) {
		this.serie = serie;
	}

	public String getSerienombre() {
		return serienombre;
	}

	public void setSerienombre(String serienombre) {
		this.serienombre = serienombre;
	}

	public int getCorrelativo() {
		return correlativo;
	}

	public void setCorrelativo(int correlativo) {
		this.correlativo = correlativo;
	}

	public String getFormapago() {
		return formapago;
	}

	public void setFormapago(String formapago) {
		this.formapago = formapago;
	}

	public LocalDateTime getFechaemision() {
		return fechaemision;
	}

	public void setFechaemision(LocalDateTime fechaemision) {
		this.fechaemision = fechaemision;
	}

	public LocalDateTime getFechavencimiento() {
		return fechavencimiento;
	}

	public void setFechavencimiento(LocalDateTime fechavencimiento) {
		this.fechavencimiento = fechavencimiento;
	}

	public MonedaDTO getMoneda() {
		return moneda;
	}

	public void setMoneda(MonedaDTO moneda) {
		this.moneda = moneda;
	}

	public double getOpgravadas() {
		return opgravadas;
	}

	public void setOpgravadas(double opgravadas) {
		this.opgravadas = opgravadas;
	}

	public double getOpexoneradas() {
		return opexoneradas;
	}

	public void setOpexoneradas(double opexoneradas) {
		this.opexoneradas = opexoneradas;
	}

	public double getOpinafectas() {
		return opinafectas;
	}

	public void setOpinafectas(double opinafectas) {
		this.opinafectas = opinafectas;
	}

	public double getIgv() {
		return igv;
	}

	public void setIgv(double igv) {
		this.igv = igv;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public String getIdtipocompref() {
		return idtipocompref;
	}

	public void setIdtipocompref(String idtipocompref) {
		this.idtipocompref = idtipocompref;
	}

	public String getSerieref() {
		return serieref;
	}

	public void setSerieref(String serieref) {
		this.serieref = serieref;
	}

	public int getCorrelativoref() {
		return correlativoref;
	}

	public void setCorrelativoref(int correlativoref) {
		this.correlativoref = correlativoref;
	}

	public String getCodmotivo() {
		return codmotivo;
	}

	public void setCodmotivo(String codmotivo) {
		this.codmotivo = codmotivo;
	}

	public String getNombrexml() {
		return nombrexml;
	}

	public void setNombrexml(String nombrexml) {
		this.nombrexml = nombrexml;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public String getEstadointerno() {
		return estadointerno;
	}

	public void setEstadointerno(String estadointerno) {
		this.estadointerno = estadointerno;
	}

	public String getXmlbase64() {
		return xmlbase64;
	}

	public void setXmlbase64(String xmlbase64) {
		this.xmlbase64 = xmlbase64;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getCodigosunat() {
		return codigosunat;
	}

	public void setCodigosunat(String codigosunat) {
		this.codigosunat = codigosunat;
	}

	public String getMensajesunat() {
		return mensajesunat;
	}

	public void setMensajesunat(String mensajesunat) {
		this.mensajesunat = mensajesunat;
	}

	public String getEstadosunat() {
		return estadosunat;
	}

	public void setEstadosunat(String estadosunat) {
		this.estadosunat = estadosunat;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public List<DetalleComprobanteDTO> getDetallecomprobante() {
		return detallecomprobante;
	}

	public void setDetallecomprobante(List<DetalleComprobanteDTO> detallecomprobante) {
		this.detallecomprobante = detallecomprobante;
	}

	public String getMetododepago() {
		return metododepago;
	}

	public void setMetododepago(String metododepago) {
		this.metododepago = metododepago;
	}

	public String getNumeroenletra() {
		return numeroenletra;
	}

	public void setNumeroenletra(String numeroenletra) {
		this.numeroenletra = numeroenletra;
	}
		
	
}
