package com.halican.dto;

import java.time.LocalDateTime;


import com.fasterxml.jackson.annotation.JsonFormat;


public class HospitalizacionDTO {

	private int idhospitalizacion;

	private LocalDateTime fechaingreso;
	
	private String horaingreso;

	private String motivoingreso;

	private String diagnostico;

	private String observaciones;

	private PacienteAnimalDTO pacienteanimal;

	public int getIdhospitalizacion() {
		return idhospitalizacion;
	}

	public void setIdhospitalizacion(int idhospitalizacion) {
		this.idhospitalizacion = idhospitalizacion;
	}

	public LocalDateTime getFechaingreso() {
		return fechaingreso;
	}

	public void setFechaingreso(LocalDateTime fechaingreso) {
		this.fechaingreso = fechaingreso;
	}

	public String getHoraingreso() {
		return horaingreso;
	}

	public void setHoraingreso(String horaingreso) {
		this.horaingreso = horaingreso;
	}

	public String getMotivoingreso() {
		return motivoingreso;
	}

	public void setMotivoingreso(String motivoingreso) {
		this.motivoingreso = motivoingreso;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public PacienteAnimalDTO getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimalDTO pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}


	
}
