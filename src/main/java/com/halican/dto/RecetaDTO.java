package com.halican.dto;

import java.time.LocalDateTime;


public class RecetaDTO {

	private int idreceta;
	
	private LocalDateTime fecha;
	
	private String detalle;
	
	private ConsultaDTO consulta;

	public int getIdreceta() {
		return idreceta;
	}

	public void setIdreceta(int idreceta) {
		this.idreceta = idreceta;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public ConsultaDTO getConsulta() {
		return consulta;
	}

	public void setConsulta(ConsultaDTO consulta) {
		this.consulta = consulta;
	} 
	
	
}
