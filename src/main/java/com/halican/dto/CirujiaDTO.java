package com.halican.dto;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.halican.Model.PacienteAnimal;

public class CirujiaDTO {

	private int idcirujia;
	
	private String intervencion;
	
	private LocalDateTime fecha;
	
	private String hora;
	
	private String observaciones;

	private PacienteAnimal pacienteanimal;

	public int getIdcirujia() {
		return idcirujia;
	}

	public void setIdcirujia(int idcirujia) {
		this.idcirujia = idcirujia;
	}

	public String getIntervencion() {
		return intervencion;
	}

	public void setIntervencion(String intervencion) {
		this.intervencion = intervencion;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public PacienteAnimal getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimal pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}
	
	
}
