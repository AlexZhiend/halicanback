package com.halican.dto;

import java.time.LocalDateTime;

public class TratamientoDTO {

	private int idtratamiento;
	
	private String descripcion;
	
	private LocalDateTime fechainicio;
	
	private LocalDateTime fechafin;
	
	private String estado;
	
	private String observaciones;
	
	private PacienteAnimalDTO pacienteanimal;

	public int getIdtratamiento() {
		return idtratamiento;
	}

	public void setIdtratamiento(int idtratamiento) {
		this.idtratamiento = idtratamiento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDateTime getFechainicio() {
		return fechainicio;
	}

	public void setFechainicio(LocalDateTime fechainicio) {
		this.fechainicio = fechainicio;
	}

	public LocalDateTime getFechafin() {
		return fechafin;
	}

	public void setFechafin(LocalDateTime fechafin) {
		this.fechafin = fechafin;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public PacienteAnimalDTO getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimalDTO pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	
}
