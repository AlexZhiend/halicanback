package com.halican.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ConstantesFisiologicasDTO {
	
	private int idconstantes;

	private String consciente;

	private String hidratacion;
	
	private LocalDateTime fecha;

	private String mucosas;

	private double peso;

	private String frecuenciacardiaca;

	private String frecuenciarespiratoria;

	private String pulso;

	private String tllenadocapilar;
	
	private String temperaturas;
	
	private String descripcionexamen;

	@JsonIgnore
	private ConsultaDTO consulta;

	public int getIdconstantes() {
		return idconstantes;
	}

	public void setIdconstantes(int idconstantes) {
		this.idconstantes = idconstantes;
	}

	public String getConsciente() {
		return consciente;
	}

	public void setConsciente(String consciente) {
		this.consciente = consciente;
	}

	public String getHidratacion() {
		return hidratacion;
	}

	public void setHidratacion(String hidratacion) {
		this.hidratacion = hidratacion;
	}

	public String getMucosas() {
		return mucosas;
	}

	public void setMucosas(String mucosas) {
		this.mucosas = mucosas;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public String getFrecuenciacardiaca() {
		return frecuenciacardiaca;
	}

	public void setFrecuenciacardiaca(String frecuenciacardiaca) {
		this.frecuenciacardiaca = frecuenciacardiaca;
	}

	public String getFrecuenciarespiratoria() {
		return frecuenciarespiratoria;
	}

	public void setFrecuenciarespiratoria(String frecuenciarespiratoria) {
		this.frecuenciarespiratoria = frecuenciarespiratoria;
	}

	public String getPulso() {
		return pulso;
	}

	public void setPulso(String pulso) {
		this.pulso = pulso;
	}

	public String getTllenadocapilar() {
		return tllenadocapilar;
	}

	public void setTllenadocapilar(String tllenadocapilar) {
		this.tllenadocapilar = tllenadocapilar;
	}

	public String getTemperaturas() {
		return temperaturas;
	}

	public void setTemperaturas(String temperaturas) {
		this.temperaturas = temperaturas;
	}

	public String getDescripcionexamen() {
		return descripcionexamen;
	}

	public void setDescripcionexamen(String descripcionexamen) {
		this.descripcionexamen = descripcionexamen;
	}

	public ConsultaDTO getConsulta() {
		return consulta;
	}

	public void setConsulta(ConsultaDTO consulta) {
		this.consulta = consulta;
	}
	
	
}
