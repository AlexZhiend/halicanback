package com.halican.dto;

public class ClienteDTO {
	
	private int idcliente;
	
	private String dni;
	
	private String razonsocial;
	
	private String nombresyapellidos;
	
	private String departamento;
	
	private String provincia;
	
	private String distrito;
	
	private String direccion;
	
	private String telefono;
	
	private TipoDocumentoDTO tipodocumento;

	public int getIdcliente() {
		return idcliente;
	}

	public void setIdcliente(int idcliente) {
		this.idcliente = idcliente;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getRazonsocial() {
		return razonsocial;
	}

	public void setRazonsocial(String razonsocial) {
		this.razonsocial = razonsocial;
	}

	public String getNombresyapellidos() {
		return nombresyapellidos;
	}

	public void setNombresyapellidos(String nombresyapellidos) {
		this.nombresyapellidos = nombresyapellidos;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public TipoDocumentoDTO getTipodocumento() {
		return tipodocumento;
	}

	public void setTipodocumento(TipoDocumentoDTO tipodocumento) {
		this.tipodocumento = tipodocumento;
	}
	
	

}
