package com.halican.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ReconsultaDTO {

	private	int idreconsulta;
	
	private String motivo;
	
	private LocalDateTime fecha;
	
	private String signossintomas;
	
	private ConsultaDTO consulta;

	public int getIdreconsulta() {
		return idreconsulta;
	}

	public void setIdreconsulta(int idreconsulta) {
		this.idreconsulta = idreconsulta;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getSignossintomas() {
		return signossintomas;
	}

	public void setSignossintomas(String signossintomas) {
		this.signossintomas = signossintomas;
	}

	public ConsultaDTO getConsulta() {
		return consulta;
	}

	public void setConsulta(ConsultaDTO consulta) {
		this.consulta = consulta;
	} 
	
	
	
}
