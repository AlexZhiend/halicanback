package com.halican.dto;


import java.time.LocalDateTime;

public class DetalleExamenDTO {

	private int iddetalleexamen;

	private String estado;

	private LocalDateTime fecha;

	private LocalDateTime fechaentrega;

	private ExamenMedicoDTO examenmedico;

	private ClienteDTO cliente;



	public int getIddetalleexamen() {
		return iddetalleexamen;
	}

	public void setIddetalleexamen(int iddetalleexamen) {
		this.iddetalleexamen = iddetalleexamen;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public LocalDateTime getFechaentrega() {
		return fechaentrega;
	}

	public void setFechaentrega(LocalDateTime fechaentrega) {
		this.fechaentrega = fechaentrega;
	}

	public ExamenMedicoDTO getExamenmedico() {
		return examenmedico;
	}

	public void setExamenmedico(ExamenMedicoDTO examenmedico) {
		this.examenmedico = examenmedico;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
	
	
}
