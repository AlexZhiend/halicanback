package com.halican.dto;

import java.time.LocalDateTime;

public class RegistroDesparasitacionDTO {

	private int idregdesparasitacion;
	
	private LocalDateTime fechaaplicacion;
	
	private LocalDateTime fechasiguiente;
	
	private double peso;
	
	private double cantidad;
	
	private String medicamento;
	
	private String observaciones;
	
	private PacienteAnimalDTO pacienteanimal;

	public int getIdregdesparasitacion() {
		return idregdesparasitacion;
	}

	public void setIdregdesparasitacion(int idregdesparasitacion) {
		this.idregdesparasitacion = idregdesparasitacion;
	}

	public LocalDateTime getFechaaplicacion() {
		return fechaaplicacion;
	}

	public void setFechaaplicacion(LocalDateTime fechaaplicacion) {
		this.fechaaplicacion = fechaaplicacion;
	}

	public LocalDateTime getFechasiguiente() {
		return fechasiguiente;
	}

	public void setFechasiguiente(LocalDateTime fechasiguiente) {
		this.fechasiguiente = fechasiguiente;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public String getMedicamento() {
		return medicamento;
	}

	public void setMedicamento(String medicamento) {
		this.medicamento = medicamento;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public PacienteAnimalDTO getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimalDTO pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}


	
}
