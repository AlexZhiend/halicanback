package com.halican.dto;

public class EstadoInternoDTO {

	private int idestadointerno;

	private String descripcion;

	public int getIdestadointerno() {
		return idestadointerno;
	}

	public void setIdestadointerno(int idestadointerno) {
		this.idestadointerno = idestadointerno;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
