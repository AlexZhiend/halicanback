package com.halican.dto;

public class SerieDTO {

	private int idserie;
		
	private String serie;
	
	private int correlativo;
	
	private TipoComprobanteDTO tipoComprobante;

	public int getIdserie() {
		return idserie;
	}

	public void setIdserie(int idserie) {
		this.idserie = idserie;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public int getCorrelativo() {
		return correlativo;
	}

	public void setCorrelativo(int correlativo) {
		this.correlativo = correlativo;
	}

	public TipoComprobanteDTO getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(TipoComprobanteDTO tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}


	
	
}
