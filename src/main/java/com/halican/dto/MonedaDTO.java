package com.halican.dto;

public class MonedaDTO {

	private int idmoneda;

	private String descripcion;

	public int getIdmoneda() {
		return idmoneda;
	}

	public void setIdmoneda(int idmoneda) {
		this.idmoneda = idmoneda;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
