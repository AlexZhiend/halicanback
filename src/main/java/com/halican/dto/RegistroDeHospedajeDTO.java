package com.halican.dto;

import java.time.LocalDateTime;

public class RegistroDeHospedajeDTO {
	
	private	int idregistrohospedaje;
	
	private LocalDateTime fechaentrada;
	
	private LocalDateTime fechasalida;
	
	private String estadoanimal;
	
	private String alimento;
	
	private String observaciones;
	
	private PacienteAnimalDTO pacienteanimal;

	public int getIdregistrohospedaje() {
		return idregistrohospedaje;
	}

	public void setIdregistrohospedaje(int idregistrohospedaje) {
		this.idregistrohospedaje = idregistrohospedaje;
	}

	public LocalDateTime getFechaentrada() {
		return fechaentrada;
	}

	public void setFechaentrada(LocalDateTime fechaentrada) {
		this.fechaentrada = fechaentrada;
	}

	public LocalDateTime getFechasalida() {
		return fechasalida;
	}

	public void setFechasalida(LocalDateTime fechasalida) {
		this.fechasalida = fechasalida;
	}

	public String getEstadoanimal() {
		return estadoanimal;
	}

	public void setEstadoanimal(String estadoanimal) {
		this.estadoanimal = estadoanimal;
	}

	public String getAlimento() {
		return alimento;
	}

	public void setAlimento(String alimento) {
		this.alimento = alimento;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public PacienteAnimalDTO getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimalDTO pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}

	
}
