package com.halican.dto;


public class CategoriaProductoDTO {
	
	private int idcategoriaproducto;
	private String nombre;
	
	public int getIdcategoriaproducto() {
		return idcategoriaproducto;
	}
	public void setIdcategoriaproducto(int idcategoriaproducto) {
		this.idcategoriaproducto = idcategoriaproducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}
