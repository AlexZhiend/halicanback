package com.halican.dto;

import java.time.LocalDateTime;
import java.util.List;

public class ConsultaDTO {
	
	private Integer idconsulta;

	private LocalDateTime fecha;

	private String motivo;

	private String inicio;
	
	private String signossintomas;
	
	private String receta;
	
	private PacienteAnimalDTO pacienteanimal;

	private PersonalVeterinarioDTO personalveterinario;
	
	private List<ConstantesFisiologicasDTO> constantesfisiologicas;

	public Integer getIdconsulta() {
		return idconsulta;
	}

	public void setIdconsulta(Integer idconsulta) {
		this.idconsulta = idconsulta;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getInicio() {
		return inicio;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public String getSignossintomas() {
		return signossintomas;
	}

	public void setSignossintomas(String signossintomas) {
		this.signossintomas = signossintomas;
	}

	public PacienteAnimalDTO getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimalDTO pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}

	public PersonalVeterinarioDTO getPersonalveterinario() {
		return personalveterinario;
	}

	public void setPersonalveterinario(PersonalVeterinarioDTO personalveterinario) {
		this.personalveterinario = personalveterinario;
	}

	public String getReceta() {
		return receta;
	}

	public void setReceta(String receta) {
		this.receta = receta;
	}

	public List<ConstantesFisiologicasDTO> getConstantesfisiologicas() {
		return constantesfisiologicas;
	}

	public void setConstantesfisiologicas(List<ConstantesFisiologicasDTO> constantesfisiologicas) {
		this.constantesfisiologicas = constantesfisiologicas;
	}
	
	
	
}
