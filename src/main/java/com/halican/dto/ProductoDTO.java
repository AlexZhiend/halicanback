package com.halican.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.halican.Model.CategoriaProducto;

public class ProductoDTO {

	private int idproducto;

	private String nombre;
	
	private String codigobarras;
	
	private LocalDate fechavencimiento;
	
	private int cantidad;
	
	private int stockinicial;
	
	private String presentacion;
	
	private double precioingreso;
	
	private String marcaproducto;
	
	private String loteproducto;
	
	private LocalDate fechaingreso;
	 
	private double valorunitario;
	
	private TipoAfectacionDTO tipoafectacion;
	
	private UnidadDTO unidad;
	
	private ProveedorDTO proveedor;

	private CategoriaProductoDTO categoriaproducto;
	
	private String subcategoria;

	public int getIdproducto() {
		return idproducto;
	}

	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigobarras() {
		return codigobarras;
	}

	public void setCodigobarras(String codigobarras) {
		this.codigobarras = codigobarras;
	}

	public LocalDate getFechavencimiento() {
		return fechavencimiento;
	}

	public void setFechavencimiento(LocalDate fechavencimiento) {
		this.fechavencimiento = fechavencimiento;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getStockinicial() {
		return stockinicial;
	}

	public void setStockinicial(int stockinicial) {
		this.stockinicial = stockinicial;
	}

	public String getPresentacion() {
		return presentacion;
	}

	public void setPresentacion(String presentacion) {
		this.presentacion = presentacion;
	}

	public double getPrecioingreso() {
		return precioingreso;
	}

	public void setPrecioingreso(double precioingreso) {
		this.precioingreso = precioingreso;
	}

	public String getMarcaproducto() {
		return marcaproducto;
	}

	public void setMarcaproducto(String marcaproducto) {
		this.marcaproducto = marcaproducto;
	}

	public String getLoteproducto() {
		return loteproducto;
	}

	public void setLoteproducto(String loteproducto) {
		this.loteproducto = loteproducto;
	}

	public LocalDate getFechaingreso() {
		return fechaingreso;
	}

	public void setFechaingreso(LocalDate fechaingreso) {
		this.fechaingreso = fechaingreso;
	}

	public double getValorunitario() {
		return valorunitario;
	}

	public void setValorunitario(double valorunitario) {
		this.valorunitario = valorunitario;
	}

	public TipoAfectacionDTO getTipoafectacion() {
		return tipoafectacion;
	}

	public void setTipoafectacion(TipoAfectacionDTO tipoafectacion) {
		this.tipoafectacion = tipoafectacion;
	}

	public UnidadDTO getUnidad() {
		return unidad;
	}

	public void setUnidad(UnidadDTO unidad) {
		this.unidad = unidad;
	}

	public ProveedorDTO getProveedor() {
		return proveedor;
	}

	public void setProveedor(ProveedorDTO proveedor) {
		this.proveedor = proveedor;
	}

	public CategoriaProductoDTO getCategoriaproducto() {
		return categoriaproducto;
	}

	public void setCategoriaproducto(CategoriaProductoDTO categoriaproducto) {
		this.categoriaproducto = categoriaproducto;
	}

	public String getSubcategoria() {
		return subcategoria;
	}

	public void setSubcategoria(String subcategoria) {
		this.subcategoria = subcategoria;
	}
		
	
	
}
