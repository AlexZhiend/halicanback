package com.halican.dto;

public class TipoComprobanteDTO {

	private int idtipocomprobante;
		
	private String nomenclatura;
	
	private String codigotipocomprobante;

	public int getIdtipocomprobante() {
		return idtipocomprobante;
	}

	public void setIdtipocomprobante(int idtipocomprobante) {
		this.idtipocomprobante = idtipocomprobante;
	}

	public String getNomenclatura() {
		return nomenclatura;
	}

	public void setNomenclatura(String nomenclatura) {
		this.nomenclatura = nomenclatura;
	}

	public String getCodigotipocomprobante() {
		return codigotipocomprobante;
	}

	public void setCodigotipocomprobante(String codigotipocomprobante) {
		this.codigotipocomprobante = codigotipocomprobante;
	}
	
	
	
}
