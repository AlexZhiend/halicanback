package com.halican.dto;

public class TipoCorteDTO {

	private int idtipocorte;

	private String descripcion;

	public int getIdtipocorte() {
		return idtipocorte;
	}

	public void setIdtipocorte(int idtipocorte) {
		this.idtipocorte = idtipocorte;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
