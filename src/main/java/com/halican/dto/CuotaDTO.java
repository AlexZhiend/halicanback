package com.halican.dto;

import java.time.LocalDateTime;

public class CuotaDTO {

	private int idcuota;

	private String numero;

	private double importe;

	private LocalDateTime fechavencimiento;

	private int estado;
	
	private ComprobanteDTO comprobante;

	public int getIdcuota() {
		return idcuota;
	}

	public void setIdcuota(int idcuota) {
		this.idcuota = idcuota;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public LocalDateTime getFechavencimiento() {
		return fechavencimiento;
	}

	public void setFechavencimiento(LocalDateTime fechavencimiento) {
		this.fechavencimiento = fechavencimiento;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public ComprobanteDTO getComprobante() {
		return comprobante;
	}

	public void setComprobante(ComprobanteDTO comprobante) {
		this.comprobante = comprobante;
	}
	
	
}
