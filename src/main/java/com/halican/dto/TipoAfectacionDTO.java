package com.halican.dto;

public class TipoAfectacionDTO {

	private int idtipoafectacion;
	
	private String descripcion;
	
	private String letra;
	
	private String codigo;
	
	private String nombre;
	
	private String tipo;

	public int getIdtipoafectacion() {
		return idtipoafectacion;
	}

	public void setIdtipoafectacion(int idtipoafectacion) {
		this.idtipoafectacion = idtipoafectacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getLetra() {
		return letra;
	}

	public void setLetra(String letra) {
		this.letra = letra;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	
}
