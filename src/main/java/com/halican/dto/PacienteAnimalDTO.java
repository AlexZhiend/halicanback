package com.halican.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class PacienteAnimalDTO {

	private int idpacienteanimal;
	
	private int numerohistoria;

	private String nombre;
	
	private String raza;
	
	private String especie;
	
	private String color;
	
	private String esterilizacion;
	
	private String sexo;
	
	private LocalDate fechanacimiento;
	
	private ClienteDTO cliente;

	public int getIdpacienteanimal() {
		return idpacienteanimal;
	}

	public void setIdpacienteanimal(int idpacienteanimal) {
		this.idpacienteanimal = idpacienteanimal;
	}

	public int getNumerohistoria() {
		return numerohistoria;
	}

	public void setNumerohistoria(int numerohistoria) {
		this.numerohistoria = numerohistoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getEsterilizacion() {
		return esterilizacion;
	}

	public void setEsterilizacion(String esterilizacion) {
		this.esterilizacion = esterilizacion;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public LocalDate getFechanacimiento() {
		return fechanacimiento;
	}

	public void setFechanacimiento(LocalDate fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
	
	
	
}
