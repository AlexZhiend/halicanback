package com.halican.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DetalleComprobanteDTO {

	private int iddetallecomprobante;

	private int cantidad;

	@JsonIgnore
	private ComprobanteDTO comprobante;

	private int item;

	private String descripcion;

	private double preciounitario;

	private double igv;

	private double porcentajeigv;

	private double valortotal;

	private double importetotal;

	private double valorunitario;

	private double descuentoitem;

	private ProductoDTO producto;

	public int getIddetallecomprobante() {
		return iddetallecomprobante;
	}

	public void setIddetallecomprobante(int iddetallecomprobante) {
		this.iddetallecomprobante = iddetallecomprobante;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public ComprobanteDTO getComprobante() {
		return comprobante;
	}

	public void setComprobante(ComprobanteDTO comprobante) {
		this.comprobante = comprobante;
	}

	public int getItem() {
		return item;
	}

	public void setItem(int item) {
		this.item = item;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPreciounitario() {
		return preciounitario;
	}

	public void setPreciounitario(double preciounitario) {
		this.preciounitario = preciounitario;
	}

	public double getIgv() {
		return igv;
	}

	public void setIgv(double igv) {
		this.igv = igv;
	}

	public double getPorcentajeigv() {
		return porcentajeigv;
	}

	public void setPorcentajeigv(double porcentajeigv) {
		this.porcentajeigv = porcentajeigv;
	}

	public double getValortotal() {
		return valortotal;
	}

	public void setValortotal(double valortotal) {
		this.valortotal = valortotal;
	}

	public double getImportetotal() {
		return importetotal;
	}

	public void setImportetotal(double importetotal) {
		this.importetotal = importetotal;
	}

	public double getValorunitario() {
		return valorunitario;
	}

	public void setValorunitario(double valorunitario) {
		this.valorunitario = valorunitario;
	}

	public double getDescuentoitem() {
		return descuentoitem;
	}

	public void setDescuentoitem(double descuentoitem) {
		this.descuentoitem = descuentoitem;
	}

	public ProductoDTO getProducto() {
		return producto;
	}

	public void setProducto(ProductoDTO producto) {
		this.producto = producto;
	}
	
	
}
