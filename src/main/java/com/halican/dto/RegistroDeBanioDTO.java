package com.halican.dto;

import java.time.LocalDateTime;

public class RegistroDeBanioDTO {
	
	private	int idregistrobanio;
	
	private LocalDateTime fecha;
	
	private String estadovisual;
	
	private String observaciones;
	
	private String estado;
	
	private TipoBanioDTO tipobanio;
	
	private TipoCorteDTO tipocorte;
	
	private PacienteAnimalDTO pacienteanimal;

	public int getIdregistrobanio() {
		return idregistrobanio;
	}

	public void setIdregistrobanio(int idregistrobanio) {
		this.idregistrobanio = idregistrobanio;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getEstadovisual() {
		return estadovisual;
	}

	public void setEstadovisual(String estadovisual) {
		this.estadovisual = estadovisual;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public TipoBanioDTO getTipobanio() {
		return tipobanio;
	}

	public void setTipobanio(TipoBanioDTO tipobanio) {
		this.tipobanio = tipobanio;
	}

	public TipoCorteDTO getTipocorte() {
		return tipocorte;
	}

	public void setTipocorte(TipoCorteDTO tipocorte) {
		this.tipocorte = tipocorte;
	}

	public PacienteAnimalDTO getPacienteanimal() {
		return pacienteanimal;
	}

	public void setPacienteanimal(PacienteAnimalDTO pacienteanimal) {
		this.pacienteanimal = pacienteanimal;
	}

	
	

}
